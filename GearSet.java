/**
 * This is the Archetype class of the program.
 * @author Team DM
 * @version 0.1
 */

import java.util.*;
import java.io.*;

public class GearSet {
	
	// FIELDS
	private String desc;
	private ArrayList<Gear> gearSetSelections;

	// CONSTRUCTORS
	public GearSet(){
		this.desc = "";
		this.gearSetSelections = new ArrayList<>();
	}

	public GearSet( String d ){
		this.desc = d;
		this.gearSetSelections = new ArrayList<>();
	}

	// METHODS
	public String GetGearSetDesc(){
		return this.desc;
	}

	public ArrayList<Gear> GetGearSetSelections(){
		return this.gearSetSelections;
	}

	public void SetGearSetDesc( String d ){
		this.desc = d;
	}

	public void SetGearSetSelections( ArrayList<Gear> gList ){
		this.gearSetSelections = gList;
	}

	public void AddGearToGearSetSelection( Gear g ){
		this.gearSetSelections.add( g );
	}

}
