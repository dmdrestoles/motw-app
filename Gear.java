/**
 * This is the Gear class of the program.
 * @author Team DM
 * @version 0.1
 */

import java.util.*;
import java.io.*;

public class Gear {
	
	// FIELDS
	private String name;
	private ArrayList<String> gearList;

	// CONSTRUCTORS
	public Gear(){
		this.name = "";
		this.gearList = new ArrayList<>();
	}

	public Gear( String n ){
		this.name = n;
		this.gearList = new ArrayList<>();
	}

	public Gear( String n, ArrayList<String> l ){
		this.name = n;
		this.gearList = l;
	}

	// METHODS
	public String GetGearName(){
		return this.name;
	}

	public ArrayList<String> GetGearList(){
		return this.gearList;
	}

	public void SetGearName( String n ){
		this.name = n;
	}

	public void SetGearList( ArrayList<String> gl ){
		this.gearList = gl;
	}

	public void AddGearToGearList( String g ){
		this.gearList.add( g );
	}
}
