/**
 * This is the Archetype class of the program.
 * @author Team DM
 * @version 0.1
 */

import java.util.*;
import java.io.*;

public class SpecialMechanicSelection {
	
	// FIELDS
	private String name;
	private ArrayList<String> selection;
	private boolean isJSONObject;

	// CONSTRUCTORS
	public SpecialMechanicSelection( boolean i ){
		this.name = "";
		this.selection = new ArrayList<>();
		this.isJSONObject = i;
	}

	public SpecialMechanicSelection( String n, boolean i ){
		this.name = n;
		this.selection = new ArrayList<>();
		this.isJSONObject = i;
	}

	// METHODS
	public String GetName(){
		return name;
	}

	/**
	 * Returns the selections available for the special mechanic selection specific.
	 * Use this for displaying the entries in the special mechanic list's selection.
	 * @return ArrayList<String> selection
	 */
	public ArrayList<String> GetSelection(){
		return selection;
	}

	public boolean IsJSONObject(){
		return isJSONObject;
	}

	public void SetName( String n ){
		name = n;
	}

	public void SetSelection( ArrayList<String> objList ){
		selection = objList;
	}

	public void SetSelection( boolean v ){
		isJSONObject = v;
	}

	public void AddEntryToSelection( String s ){
		selection.add( s );
	}

}
