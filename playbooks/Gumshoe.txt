Archetype Name:
The Gumshoe

Description:
"You won't understand this... When I take a case, I'm supposed to do something about it. You're supposed to do something about it whether you like it or not. You've got to pay for what you've done, sweetheart, whatever it  is  I might feel about you. Yes, I'll have some bad nights, but I'll still have myself."

Luck Special:
When you use a point of Luck, your next regular case will actually be a mystery for your hunter group, and not a regular case at all. (And the monster will focus its attention on you!)

Special Mechanic:
[Input] Gumshoe Code
	With the agreement of the Keeper, pick a one-sentence Code that your Gumshoe adheres to. This Code defines your Gumshoe. Any time you violate your code you forfeit all Code related moves (The Postman Always Rings Twice, The Long Goodbye) and the ability to spend Luck points. These forfeits last either until  the next mystery or you make amends. As long as you follow the Code people will sense your sincerity: you receive +1 ongoing for manipulate someone and you may not be possessed or charmed by any sort of supernatural, alien, or demonic entity or item.

//Get one move in addition to the two preselected ones
[Selection] Moves:
	Occult Confidential:
		The first time in each mystery that you observe a monster, minion, or phenomenon in action, you may ask one question from the investigate a mystery list.

	The Naked City:
		You have lots of personal contacts wherever you go. Pick four contact types from the following  areas (or from other areas agreed to between you and the Keeper): Academics, Accountants, Artists, Bartenders, Clergy, Conspiracy Theorists, Construction, Courts, Criminals (organised), Criminals (street), Cultists, Engineers, Espionage, Film and TV, Forensic Scientists, Fringe Scientists, Hackers, Journalists, Lawyers, Mechanics, Media, Medical Practioners, Military, Morgue, Occult, Police (local), Police (national), Politicians, Prisons, Private Security, Property Developers, Stage Magicians,  Technologists, Transportation
		
		You can hit them up for info (+1 to one investigate a mystery roll) or small favours—but there may be a small cost involved. Personal contacts can provide more significant help but the Keeper decides their price on a case-by-case basis.

	The Postman Always Rings Twice:
		Twice per mystery—as long as you follow your Code—you may reroll a roll.

	The Long Goodbye:
		You can't die with an open case. Specifically,  you suffer all harm as normal but your death is postponed until you have either completed or abandoned the case, or you break your Code (then all bets are off).

	Jessica Jones Entry:
		When you double-talk your way into a secure location, roll +Charm. On a 10+ pick three, on a 7-9 pick two, on a fail pick one:
			-> You don't leave any trace of what you searched.
			-> You find what you wanted.
			-> You find something else that's important.
			-> You don't piss anyone off.
			-> You aren't recognised. 

	Out of the Past:
		You have a police buddy who will do you big favors. Get in touch with them when you need to redirect law enforcement attention, get a heads-up on what operations are planned, or access police files. You now owe them: expect them to collect on it soon.

	Asphalt Jungle:
		You heal faster than normal people. Any time your harm gets healed, heal an extra point. You are immune to all the harm move effects under ‘0-harm' and ‘1-harm' (when the Keeper would apply these, you ignore it).

	Hacker with a Dragon Tattoo:
		When you hack into a computer system, roll +Sharp. On 10+ pick two, on a 7-9 pick one. You:
		-> ... leave no traces.
		-> ... learn something important.
		-> ... can leave misinformation in place.
		-> ... gain access to somewhere you want to get in to.

	"Just one more thing”:
		When you ask a suspect leading questions, roll +Charm. On a 10+ hold 2, on a 7-9 hold 1, on a miss hold 1 but something bad is going to happen too. Spend your hold to ask questions from this list:
		-> One question from the investigate a mystery list.
		-> Was that a lie?
		-> What is something you left out that you didn't want me to notice?
		-> Are you complicit with any ongoing criminal activity?
		-> Did you commit this specific crime?


Gear:
You get a laptop, a liquor flask, two recording devices, and one P.I. weapon.

[Selection] Recording devices (pick one):
	Night vision camera
	Tiny digital video camera
	Film camera (8mm or 16mm)
	Digital sound recorder
	Cassette tape recorder
	Remote-controlled camera drone
	Laser microphone
	SLR camera

[Selection] P.I. Weapons (pick one):
	Brass knuckles (1-harm hand small)
	.38 revolver (2-harm close reload loud)
	9mm (2-harm close loud)
	Magnum (3-harm close reload loud)
	Shotgun (3-harm close messy loud)
	Switchblade (1-harm hand small)


[Selection] Ratings:
	Charm +2, Cool +0, Sharp +1, Tough +0, Weird +0
	Charm +2, Cool +0, Sharp +1, Tough +1, Weird -1
	Charm +1, Cool +0, Sharp +2, Tough +1, Weird -1
	Charm +1, Cool -1, Sharp +2, Tough +0, Weird +1
	Charm +2, Cool +1, Sharp +1, Tough +0, Weird -1



[Selection] Improvements:
	Get +1 Charm (max +3)
	Get +1 Cool (max +2)
	Get +1 Sharp (max +3)
	Get +1 Tough (max +2)
	Take another Gumshoe move
	Take another Gumshoe move
	Add another harm box to your track, before Dying
	Gain a haven, like the Expert has, with two options
	Add four additional or new contacts for your Naked City move


[Selection] Advanced Improvements:
	Get +1 to any rating (max +3)
	Get back one used Luck point
	Change this Hunter to a new archetype
	Create a second Hunter to play as well as this one
	Mark two of the basic moves as advanced
	Mark two of the basic moves as advanced
	Retire this Hunter to safety
	Turn one of your contacts into an ally