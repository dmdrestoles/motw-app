Archetype Name:
The Flake

Description:
Everything's connected. But not everyone can see the patterns, and most people don't even look that hard. But me, I can never stop looking deeper. I can never stop seeing the truth. I spot the patterns. That's how I found the monsters, and that's how I help kill them.

Luck Special:
When you spend a point of Luck, pick an aspect of the current situation. The Keeper will tell you what other conspiracies that aspect connects to.

Special Mechanic:
N/A

Special Mechanic Description:
N/A

//Select three moves
Moves:
	Connect the Dots (Sharp)
		At the beginning of each mystery, if you look for the wider patterns that current events might be part of, roll +Sharp. On a 10+ hold 3, and on a 7-9 hold 1. Spend your hold during the mystery to ask the Keeper any one of the following questions:
			Is this person connected to current events more than they are saying?
			When and where will the next critical event occur?
			What does the monster want from this person?
			Is this connected to previous mysteries we have investigated?
			How does this mystery connect to the bigger picture?

	Crazy Eyes 
		You get +1 Weird (max +3).

	See, It All Fits Together (Sharp)
		You can use Sharp instead of Charm when you manipulate someone.

	Suspicious Mind 
		If someone lies to you, you know it.

	Often Overlooked (Weird)
		When you act all crazy to avoid something, roll +Weird. On a 10+ you're regarded as unthreatening and unimportant. On a 7-9, pick one: unthreatening or unimportant. On a miss, you draw lots (but not all) of the attention.

	Contrary 
		When you seek out and receive someone's honest advice on the best course of action for you and then do something else instead, mark experience. If you do exactly the opposite of their advice, you also take +1 ongoing on any moves you make pursuing that course.

	Net Friends (Charm)
		You know a lot of people on the Internet. When you contact a net friend to help you with a mystery, roll +Charm. On a 10+, they're available and helpful—they can fix something, break a code, hack a computer, or get you some special information. On a 7-9, they're prepared to help, but it's either going to take some time or you're going to have to do part of it yourself. On a miss, you burn some bridges.

	Sneaky
		When you attack from ambush, or from behind, inflict +2 harm.


[Selection] Gear:
You get one normal weapon and two hidden weapons.

	Normal weapons (pick one)
		.38 revolver (2-harm close reload loud)
		9mm (2-harm close loud)
		Hunting rifle (2-harm far loud)
		Magnum (3-harm close reload loud)
		Shotgun (3-harm close messy loud)
 		Big knife (1-harm hand)

	Hidden weapons (pick two):
		Throwing knives (1-harm close many)
		Holdout pistol (2-harm close loud reload)
		Garrote (3-harm intimate)
		Watchman's flashlight (1-harm hand)
		Weighted gloves/brass knuckles (1-harm hand)
		Butterfly knife/folding knife (1-harm hand)

[Selection] Ratings:
	Charm +1, Cool +1, Sharp +2, Tough -1, Weird +0
	Charm +0, Cool +1, Sharp +2, Tough -1, Weird +1
 	Charm +1, Cool -1, Sharp +2, Tough +1, Weird +0
	Charm +1, Cool -1, Sharp +2, Tough +0, Weird +1
	Charm -1, Cool -1, Sharp +2, Tough +0, Weird +2

Improvements:
	Get +1 Sharp, max +3
	Get +1 Charm, max +2
	Get +1 Cool, max +2
	Get +1 Weird, max +2
	Take another Flake move
	Take another Flake move
	Get a haven, like the Expert has, with two options
	Gain another option for your haven
	Take a move from another playbook
	Take a move from another playbook

Advanced Improvements:
	Get +1 to any rating, max +3.
	Change this hunter to a new type.
	Create a second hunter to play as well as this one.
	Mark two of the basic moves as advanced.
	Mark another two of the basic moves as advanced.
	Retire this hunter to safety.
	Get back one used Luck point.


