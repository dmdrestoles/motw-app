Archetype Name:
The Initiate

Description:
Since the dawn of history, we have been the bulwark against Darkness. We know the Evils of the world, and we stand against them so that the mass of humanity need not fear. We are the Flame that cleanses the Shadows.

Luck Special:
When you spend a point of Luck, some thing goes wrong for your Sect: an ill-advised project or a disastrous operation.

Special Mechanic:
Sect

Special Mechanic Description:
You are part of an ancient, secret order that slays monsters. Where are they from? How old are they? Are they religious? Why do they stay secret? How do they recruit? 

You also need to pick the Sect's traditions (these will be used by the Keeper to determine the Sect's methods and actions):

	[Selection] Good Traditions, pick two:
		Knowledgable
		Ancient lore
		Magical lore
		Fighting arts
		Modernised
		Chapters everywhere
		Secular power
		Flexible tactics
		Open hierarchy
		Integrated in society
		Rich
		Nifty gadgets
		Magical items

	[Selection] Bad Traditions, pick one:
		Dubious motives
		Tradition-bound
		Short-sighted
		Paranoid and secretive
		Closed hierarchy
		Factionalised
		Strict laws
		Mystical oaths
		Total obedience
		Tyrannical leaders
		Obsolete gear
		Poor

//select three moves aside from the preselected one
[Selection] Moves:
	[Preselected] When you are in good standing with your Sect, at the beginning of each mystery, roll +Charm. On a 10+ they provide some useful info or help in the field. On a 7-9 you get a mission associated with the mystery, and if you do it you'll get some info or help too. On a miss, they ask you to do something bad. If you fail a mission or refuse an order, you'll be in trouble with the Sect until you atone.

	Ancient Fighting Arts
		When using an old-fashioned hand weapon, you inflict +1 harm and get +1 whenever you roll protect someone.

	Mystic
		Every time you successfully use magic, take +1 forward. 

	Fortunes 
		The Sect has ancient prophecies or divination techniques to predict the future. Once per mystery, you may use them. If you look at what the future holds, roll +Weird. On a 10+ hold 3, and on a 7-9 hold 1. On a miss, you get bad information and the Keeper decides how that affects you. Spend your hold to:
			have a useful object ready.
			be somewhere you are needed, just in time.
			take +1 forward, or give +1 forward to another hunter. 
			retroactively warn someone about an attack, so that it doesn't happen. 

	Sacred Oath
		You may bind yourself to a single goal, forsaking something during your quest (e.g. speech, all sustenance but bread and water, alcohol, lying, sex, etc). Get the Keeper's agreement on this—it should match the goal in importance and difficulty. While you keep your oath and work towards your goal, mark experience at the end of every session and get +1 on any rolls that directly help achieve the goal. If you break the oath, take -1 ongoing until you have atoned.

	Mentor (Sharp)
		You have a mentor in the Sect: name them. When you contact your mentor for info, roll +Sharp. On a 10+, you get an answer to your question, no problem. On a 7-9 you choose: they're either busy and can't help, or they answer the question but you owe a favour. On a miss, your question causes trouble.

	Apprentice
		You have an apprentice: name them. Your job is to teach them the Sect's ways. They count as an ally: subordinate (motivation: to follow your instructions to the letter).

	Helping Hand
		When you successfully help out another hunter, they get +2 instead of the usual +1. 

	That Old Black Magic 
		When you use magic, you can ask a question from the investigate a mystery move as your effect.

Gear:
If your Sect has fighting arts or obsolete gear then pick three old-fashioned weapons. If the Sect has modernised or nifty gadgets, you may pick two modern weapons. Otherwise, pick two old-fashioned weapons and one modern weapon. You also get old-fashioned armour (1-armour heavy). 

[Selection] Old-fashioned weapons, pick either two or three, as above:
	Sword (2-harm hand messy)
	Axe (2-harm hand messy)
	Big sword (3-harm hand messy heavy)
	Big axe (3-harm hand messy slow heavy)
	Silver knife (1-harm hand silver)
	Fighting sticks (1-harm hand quick)
	Spear (2-harm hand/close)
	Mace (2-harm hand messy)
	Crossbow (2-harm close slow)	

[Selection] Modern weapons, pick either one or two, as above:
	.38 revolver (2-harm close reload loud)
	9mm (2-harm close loud)
	Sniper rifle (3-harm far)
	Magnum (3-harm close reload loud)
	Shotgun (3-harm close messy)

[Selection] Ratings:
	Charm -1, Cool +1, Sharp +0, Tough +1, Weird +2
	Charm +0, Cool +1, Sharp +1, Tough -1, Weird +2
	Charm -1, Cool +0, Sharp -1, Tough +2, Weird +2
	Charm +1, Cool -1, Sharp +1, Tough +0, Weird +2
	Charm +0, Cool +0, Sharp +0, Tough +1, Weird +2

[Selection] Improvements:
	Get +1 Weird, max +3 
	Get +1 Cool, max +2
	Get +1 Sharp, max +2
	Get +1 Tough, max +2
	Take another Initiate move
	Take another Initiate move
	Get command of your chapter of the Sect
	Get a Sect team under your command
	Take a move from another playbook
	Take a move from another playbook

[Selection] Advanced Improvements:
	Get +1 to any rating, max +3.
	Change this hunter to a new type.
	Create a second hunter to play as well as this one.
	Mark two of the basic moves as advanced.
	Mark another two of the basic moves as advanced.
	Retire this hunter to safety.
	Become the leader, or effective leader, of the whole Sect.
	Get back one used Luck point.