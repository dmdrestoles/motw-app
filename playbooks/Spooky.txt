Archetype Name:
The Spooky

Description:
I can do things, things that normal people can't. But there's a price—I haven't paid it in full, yet, but the bill's gonna come due soon. It's best I don't tell you any more. You get too close, you'll get hurt.

Luck Special:
As you mark off Luck boxes, your Dark Side's needs will get nastier.

Special Mechanic:
The Dark Side
	Your powers have an unsavory source, and sometimes you get tempted to do things you shouldn't. These could be orders from whatever granted your power, or urges that bubble up from your subconscious. Something like that. Whatever it is, it's unsettling.

	The Keeper can ask you to do nasty things (in accordance with the tags), when your powers need you to. If you do whatever is asked, mark experience. If you don't do it, then your powers are unavailable until the end of the mystery (or until you cave). As you mark off Luck boxes, these requests will get bigger and nastier

	[Selection] Pick three tags for your dark side:
		Violence
		Depression
		Secrets
		Lust
		Dark bargain
		Guilt
		Soulless
		Addiction
		Mood swings
		Rage
		Self-destruction
		Greed for power
		Poor impulse control
		Hallucinations
		Pain
		Paranoia

[Selection] Moves:
	Telepathy:
		You canread people's thoughts and put words in their mind. This can allow you to investigate a mystery or read a bad situation without needing to actually talk. You can also manipulate someone without speaking. You still roll moves as normal, except people will not expect the weirdness of your mental communication.

	Hex:
		When you cast a spell (with use magic), as well as the normal effects, you may pick from the follow-ing:
			-> The target contracts a disease.
			-> The target immediately suffers harm (2-harm magic ignore-armour).
			-> The target breaks something precious or important.

	The Sight:
		You can see the invisible, especially spirits and magical influences. You may communicate with (maybe even make deals with) the spirits you see, and they give you more opportunities to spot clues when you investigate a mystery.

	Premonitions: (Weird)
		At the start of each mystery, roll +Weird. On a 10+, you get a detailed vision of something bad that is yet to happen. You take +1 forward to prevent it coming true, and mark experience if you stop it. On a 7-9+ you get clouded images of some-thing bad that is yet to happen: mark experience if you stop it. On a miss, you get a vision of something bad happening to you and the Keeper holds 3, to be spent one-for-one as penalties to rolls you make.

	Hunches: (Sharp)
		When something bad is happening (or just about to happen) somewhere that you aren't, roll +Sharp. On a 10+ you knew where you needed to go, just in time to get there. On a 7-9, you get there late—in time to intervene, but not prevent it altogether. On a miss, you get there just in time to be in trouble yourself.

	Tune In: (Weird)
		You can attune your mind to a monster or minion. Roll +Weird. On a 10+, hold 3. On a 7-9, hold 1. On a miss, the monster becomes aware of you. Spend one hold to ask the Keeper one of the following questions, and gain +1 ongoing while acting on the answers:
			-> Where is the creature right now?
			-> What is it planning to do right now?
			-> Who is it going to attack next?
			-> Who does it regard as the biggest threat?
			-> How can I attract its attention?

	The Big Whammy:
		You can use your powers to kick some ass: roll +Weird instead of +Tough. The attack has 2-harm close obvious ignore-armour. On a miss, you'll get a magical backlash.

	Jinx: (Weird)
		You can encourage coincidences to occur, the way you want. When you jinx a target, roll +Weird. On a 10+ hold 2 and on a 7-9 hold 1. On a miss, the Keeper holds 2 over you to be used in the same way. Spend your hold to:
			-> Interfere with a hunter, giving them -1 forward.
			-> Help a hunter, giving them +1 forward, by interfering with their enemy.
			-> Interfere with what a monster, minion, or bystander is trying to do.
			-> Inflict 1-harm on the target due to an accident.
			-> The target finds something you left for them.
			-> The target loses something that you will soon find


[Selection] Gear:
	.38 revolver (2-harm close reload loud)
	9mm (2-harm close loud)
	Hunting rifle (2-harm far loud)
	Shotgun (3-harm close messy)
	Big knife (1-harm hand)

[Selection] Ratings:
	Charm +1, Cool +0, Sharp +1, Tough -1, Weird +2
	Charm -1, Cool +1, Sharp +0, Tough +1, Weird +2
	Charm +2, Cool +0, Sharp -1, Tough -1, Weird +2
	Charm +0, Cool -1, Sharp +1, Tough +1, Weird +2
	Charm -1, Cool -1, Sharp +2, Tough +0, Weird +2



[Selection] Improvements:
	Get +1 Charm (max +2)
	Get +1 Cool (max +2)
	Get +1 Sharp (max +2)
	Get +1 Weird (max +3)
	Take another Spooky move
	Take another Spooky move
	Take a move from another playbook
	Take a move from another playbook
	Change some, or all of your Dark Side tags
	Get a mystical library, like the Expert's haven option


[Selection] Advanced Improvements:
	Get +1 to any rating (max +3)
	Get back one used Luck point
	Change this Hunter to a new archetype
	Create a second Hunter to play as well as this one
	Mark two of the basic moves as advanced
	Mark two of the basic moves as advanced
	Retire this Hunter to safety
	You discover how to use your powers at a lower price. Delete on dark side tag permanently.