/**
 * This is the Archetype class of the program.
 * @author Team DM
 * @version 0.1
 */

import java.util.*;
import java.io.*;

public class Archetype {

	// FIELDS
	private String name;
	private String desc;
	private String luckSpecial;

	private ArrayList<SpecialMechanic> specialMechanics;
	private MoveList moveList;
	private GearSet gearSet;
	private ArrayList<Rating> ratings;
	private ArrayList<String> improvements;
	private ArrayList<String> advancedImprovements;

	// CONSTRUCTORS
	public Archetype(){
		name = "Archetype";
		desc = "Description";
		luckSpecial = "Luck special";
		specialMechanics = new ArrayList<>();
		moveList = new MoveList();
		gearSet = new GearSet();
		ratings = new ArrayList<>();
		improvements = new ArrayList<>();
		advancedImprovements = new ArrayList<>();
	}

	public Archetype( String n, String d ){
		name = n;
		desc = d;
		specialMechanics = new ArrayList<>();
		moveList = new MoveList();
		gearSet = new GearSet();
		ratings = new ArrayList<>();
		improvements = new ArrayList<>();
		advancedImprovements = new ArrayList<>();
	}

	// METHODS
	public String GetArchetypeName(){
		return name;
	}

	public String GetArchetypeDesc(){
		return desc;
	}

	public String GetLuckSpecial(){
		return luckSpecial;
	}

	public ArrayList<SpecialMechanic> GetSpecialMechanics(){
		return specialMechanics;
	}

	public MoveList GetMoveList(){
		return moveList;
	}

	public GearSet GetGearSet(){
		return gearSet;
	}

	public ArrayList<Rating> GetRatings(){
		return ratings;
	}

	public ArrayList<String> GetImprovements(){
		return improvements;
	}

	public ArrayList<String> GetAdvancedImprovements(){
		return advancedImprovements;
	}

	public void SetArchetypeName( String n ){
		name = n;
	}

	public void SetArchetypeDesc( String d ){
		desc = d;
	}

	public void SetArchetypeLuckSpecial( String d ){
		luckSpecial = d;
	}

	public void SetSpecialMechanics( ArrayList<SpecialMechanic> objList ){
		specialMechanics = objList;
	}

	public void SetMoveList( MoveList ml ){
		moveList = ml;
	}

	public void SetGearSet( GearSet gs ){
		gearSet = gs;
	}

	public void SetRatings( ArrayList<Rating> objList ){
		ratings = objList;
	}

	public void SetImprovements( ArrayList<String> objList ){
		improvements = objList;
	}

	public void SetAdvancedImprovements( ArrayList<String> objList ){
		advancedImprovements = objList;
	}

	public void AddSpecialMechanic( SpecialMechanic obj ){
		specialMechanics.add( obj );
	}

	public void AddRating( Rating obj ){
		ratings.add( obj );
	}

	public void AddImprovement( String s ){
		improvements.add( s );
	}

	public void AddAdvancedImprovement( String s ){
		advancedImprovements.add( s );
	}
	
}
