import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import javax.swing.*;
import java.io.*;
import java.util.*;
import javax.imageio.ImageIO;

public class CharacterCreationGUI extends JFrame
{
    public JScrollPane scrollPane;
    public JRadioButton ratingSelection;
    public ButtonGroup ratingGroup;
    public JPanel contentPanelContainer, contentPanel, topPanel, topPanelInner, characterDescription, ratings, gear, moves, savePanel;
    public JLabel characterImage, charDescription, ratingLabel, gearLabel, gearText, moveLabel;
    public JButton saveCharacter, cancel, returnButton;
    public JTextField charName;
    public JTextArea descriptionText, gearDescLabel, gearEntries, gearInput, moveDescLabel, moveEntries, moveInput;
    public JComboBox archetypeSelection;
    public ArrayList<JTextArea> specialMechanicsInput;
    public ArrayList<JCheckBox> moveCheckList;
    public String[] archetypeNames = {"Choose an Archetype", "The Chosen", "The Crooked", "The Divine", "The Expert", "The Flake", "The Gumshoe", "The Hex", "The Initiate", "The Monstrous", "The Mundane", "The Pararomantic", "The Professional", "The Searcher", "The Spooky", "The Spell-Slinger", "The Wronged"};
    public String comboBoxString;
    public Color dBlue, lBlue, dViolet, lViolet;
    public Font ptSansSmall, ptSansBig, ptSansItalic, ptSansRegular, cantarellS;
    public FileLoader fl;
    public Archetype currentArchetype;
    public Container Screen;

    private static int COLUMNSIZE = 30;

    public CharacterCreationGUI()
    {
        dBlue = new Color(33, 35, 44);
        lBlue = new Color(87, 93, 117);

        ptSansSmall = new Font("PT Sans", Font.BOLD, 20);
        ptSansBig = new Font("PT Sans", Font.BOLD, 36);
        ptSansItalic = new Font("PT Sans", Font.ITALIC, 24);
        ptSansRegular = new Font("PT Sans", Font.PLAIN, 14);

        fl = new FileLoader();
        try {
			fl.VerifyArchetype();
		}
		catch (IOException ioe){
			ioe.printStackTrace();
		}

        currentArchetype = new Archetype();
        ratingGroup = new ButtonGroup();
        moveCheckList = new ArrayList<>();

        Screen = this.getContentPane();

        characterCreationScreen();
    }

    public void characterCreationScreen()
    {
        contentPanelContainer = new JPanel();
        contentPanelContainer.setLayout(new BoxLayout(contentPanelContainer, BoxLayout.Y_AXIS));
        contentPanel = new JPanel();
        contentPanel.setLayout(new BoxLayout(contentPanel, BoxLayout.Y_AXIS));
        topPanel = new JPanel();
        topPanel.setLayout( new FlowLayout( FlowLayout.LEFT ) );
        characterDescription = new JPanel(new BorderLayout());
        ratings = new JPanel();
        ratings.setLayout( new BoxLayout( ratings, BoxLayout.Y_AXIS ) ); 
        gear = new JPanel();
        gear.setLayout( new BoxLayout( gear, BoxLayout.Y_AXIS ) );
        moves = new JPanel();
        moves.setLayout( new BoxLayout( moves, BoxLayout.Y_AXIS ) );
        savePanel = new JPanel();
        savePanel.setLayout( new BoxLayout( savePanel, BoxLayout.Y_AXIS ) );

        contentPanel.setBackground(dBlue);
        topPanel.setBackground(dBlue);
        characterDescription.setBackground(dBlue);
        ratings.setBackground(dBlue);
        gear.setBackground(dBlue);
        moves.setBackground(dBlue);
        savePanel.setBackground(dBlue);

        contentPanelContainer.setAlignmentX( Component.LEFT_ALIGNMENT );
        contentPanel.setAlignmentX( Component.LEFT_ALIGNMENT );
        topPanel.setAlignmentX( Component.LEFT_ALIGNMENT );
        characterDescription.setAlignmentX( Component.LEFT_ALIGNMENT );
        ratings.setAlignmentX( Component.LEFT_ALIGNMENT );
        gear.setAlignmentX( Component.LEFT_ALIGNMENT );
        moves.setAlignmentX( Component.LEFT_ALIGNMENT );

        contentPanel.setAlignmentY( Component.LEFT_ALIGNMENT );
        topPanel.setAlignmentY( Component.LEFT_ALIGNMENT );
        characterDescription.setAlignmentY( Component.LEFT_ALIGNMENT );
        ratings.setAlignmentY( Component.LEFT_ALIGNMENT );
        gear.setAlignmentY( Component.LEFT_ALIGNMENT );
        moves.setAlignmentY( Component.LEFT_ALIGNMENT );

        scrollPane = new JScrollPane();
        scrollPane.setViewportView(contentPanelContainer);
        scrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        scrollPane.setPreferredSize(new Dimension(620, 950));
        scrollPane.getVerticalScrollBar().setValue( 0 );
        scrollPane.getViewport().setViewPosition( new Point( 0, 0 ) );
        topPanel.setPreferredSize(new Dimension(600, 120));
        
        returnButton = new JButton("<<< Return to Character Select");
        returnButton.setBackground(lViolet);
        returnButton.setForeground(Color.WHITE);
        returnButton.setFont(cantarellS);
        returnButton.setAlignmentX( Component.LEFT_ALIGNMENT );
    
        charName = new JTextField("Character Name");
        charName.setEditable(true);
        charName.setBackground(dBlue);
        charName.setForeground(Color.WHITE);
        charName.setFont(ptSansBig);
        charName.setPreferredSize(new Dimension(600, 40));

        archetypeSelection = new JComboBox(archetypeNames);
        archetypeSelection.setBackground(lBlue);
        archetypeSelection.setForeground(Color.WHITE);
        archetypeSelection.setFont(ptSansRegular);
        archetypeSelection.setPreferredSize(new Dimension(600, 20));

        topPanel.add( returnButton );
        topPanel.add( charName );
        topPanel.add( archetypeSelection );

        class ReturnButton implements ActionListener
        {
            public void actionPerformed(ActionEvent ae)
            {
                JFrame launchScreen = new LaunchScreenGUI();
                launchScreen.setSize(1440, 1024);
                launchScreen.setTitle("Monster of the Week Hunter Companion");
                launchScreen.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                launchScreen.setVisible(true);
                dispose();
                revalidate();
                repaint();
            }
        }
        returnButton.addActionListener(new ReturnButton());

        //Contents of CharacterDescription JPanel
        charDescription = new JLabel("Character Description");
        charDescription.setFont(ptSansSmall);
        charDescription.setBackground(dBlue);
        charDescription.setForeground(Color.WHITE);

        descriptionText = new JTextArea();
        descriptionText.setRows( 8 );
        descriptionText.setLineWrap(true);
        descriptionText.setWrapStyleWord(true);
        descriptionText.setFont(ptSansRegular);

        class MoveChecklistListener implements ItemListener{

            private int MAX_SELECTIONS;
            private int selectionCounter = 0;

            MoveChecklistListener( int i ){
                MAX_SELECTIONS = i;
            }

            public void adjustMaxSelections( int c ){
                MAX_SELECTIONS = c;
            }

            public int getMaxSelections(){
                return this.MAX_SELECTIONS;
            }

            @Override
            public void itemStateChanged( ItemEvent e ){
                JCheckBox source = ( JCheckBox ) e.getSource();
                if ( source.isSelected() ){
                    selectionCounter++;

                    if ( selectionCounter == MAX_SELECTIONS ){
                        for ( JCheckBox box : moveCheckList ){
                            if ( !box.isSelected() ){
                                box.setEnabled( false );
                            }
                        }
                    }
                }
                else{
                    selectionCounter--;
                    
                    if( selectionCounter < MAX_SELECTIONS ){
                        for ( JCheckBox box : moveCheckList ){
                            box.setEnabled( true );
                        }
                    }
                }
            }

        }
        //get Archetype from ComboBox
        class getComboBoxInput implements ActionListener
        {
            public void actionPerformed(ActionEvent e)
            {
                ratings.removeAll();
                gear.removeAll();
                moves.removeAll();
                revalidate();
                repaint();

                characterDescription.add(charDescription, BorderLayout.NORTH);
                characterDescription.add(descriptionText, BorderLayout.CENTER);
                
                JLabel emptyLabel = new JLabel(" \n");
                //Contents of Ratings
                ratingLabel = new JLabel("Ratings");
                ratingLabel.setBackground(dBlue);
                ratingLabel.setForeground(Color.WHITE);
                ratingLabel.setFont(ptSansSmall);

                ratings.add(emptyLabel);
                ratings.add(ratingLabel);

                comboBoxString = (String)archetypeSelection.getSelectedItem();
                for (Archetype a: fl.GetArchetypes())
                {
                    if (comboBoxString.equals(a.GetArchetypeName()))
                    {
                        currentArchetype = a;
                    }
                }

                for (Rating r: currentArchetype.GetRatings())
                {
                    ratingSelection = new JRadioButton(r.GetRatingName());
                    ratingSelection.setBackground(dBlue);
                    ratingSelection.setForeground(Color.WHITE);
                    ratingSelection.setFont(ptSansRegular);
                    ratingGroup.add(ratingSelection);
                    ratings.add(ratingSelection);
                }

                //Contents of Gear
                JTextArea gearLabel = new JTextArea("Gear");
                gearLabel.setEditable( false );
                gearLabel.setLineWrap(true);
                gearLabel.setWrapStyleWord(true);
                gearLabel.setBackground(dBlue);
                gearLabel.setForeground(Color.WHITE);
                gearLabel.setFont(ptSansSmall);

                gear.add( emptyLabel );
                gear.add( gearLabel ); 

                gearDescLabel = new JTextArea(currentArchetype.GetGearSet().GetGearSetDesc());
                gearDescLabel.setEditable( false );
                gearDescLabel.setBackground(dBlue);
                gearDescLabel.setForeground(Color.WHITE);
                gearDescLabel.setFont(ptSansRegular);
                gearDescLabel.setLineWrap(true);
                gearDescLabel.setWrapStyleWord(true);

                gear.add(gearDescLabel);

                gearEntries = new JTextArea();
                gearEntries.setColumns( COLUMNSIZE );
                gearEntries.setEditable( false );
                gearEntries.setBackground(dBlue);
                gearEntries.setForeground(Color.WHITE);
                gearEntries.setFont(ptSansRegular);
                gearEntries.setLineWrap(true);
                gearEntries.setWrapStyleWord(true);
                for (Gear g: currentArchetype.GetGearSet().GetGearSetSelections())
                {
                    gearEntries.append( "\n" + g.GetGearName() + "\n" );

                    for(String gearString: g.GetGearList())
                    {
                        gearEntries.append( "-> " + gearString + "\n" );
                    }
                }

                gear.add(gearEntries);

                gearInput = new JTextArea();
                gearInput.setRows(5);
                gearInput.setBackground(Color.WHITE);
                gearInput.setFont(ptSansRegular);
                gearInput.setLineWrap(true);
                gearInput.setWrapStyleWord(true);

                gear.add( gearInput );

                // Content of Moves
                JTextArea moveLabel = new JTextArea("Moves");
                moveLabel.setEditable( false );
                moveLabel.setLineWrap(true);
                moveLabel.setWrapStyleWord(true);
                moveLabel.setBackground(dBlue);
                moveLabel.setForeground(Color.WHITE);
                moveLabel.setFont(ptSansSmall);

                moves.add( emptyLabel );
                moves.add(moveLabel);

                MoveList moveList = currentArchetype.GetMoveList();

                moveDescLabel = new JTextArea( moveList.GetMovesDesc() );
                moveDescLabel.append( "\n" );
                moveDescLabel.setEditable( false );
                moveDescLabel.setBackground(dBlue);
                moveDescLabel.setForeground(Color.WHITE);
                moveDescLabel.setFont(ptSansRegular);
                moveDescLabel.setLineWrap(true);
                moveDescLabel.setWrapStyleWord(true);

                moves.add( moveDescLabel );

                JPanel moveEntries = new JPanel();
                moveEntries.setLayout( new BoxLayout( moveEntries, BoxLayout.Y_AXIS ) );
                moveEntries.setBackground( dBlue );

                MoveChecklistListener moveChecklistListener = new MoveChecklistListener( moveList.GetMaxMoveCount() );

                for ( Move m : moveList.GetMoveList() ){
                    JPanel moveEntryPanel = new JPanel( new BorderLayout() );
                    moveEntryPanel.setBackground( dBlue );
                    
                    String moveEntryLabel = m.GetMoveName();
                    JCheckBox moveEntry = new JCheckBox( moveEntryLabel );
                    moveEntry.setAlignmentX( JCheckBox.LEFT_ALIGNMENT );
                    moveEntry.setBackground(dBlue);
                    moveEntry.setForeground(Color.WHITE);
                    moveEntry.setFont(ptSansRegular);
                    
                    if( m.IsPreselected() ){
                        moveEntry.setSelected( true );
                        moveEntry.setEnabled( false );
                    }
                    else{ 
                        moveCheckList.add( moveEntry );
                        moveEntry.addItemListener( moveChecklistListener );
                    }

                    String moveEntryDesc = m.GetMoveDesc();
                    JTextArea moveDesc = new JTextArea( moveEntryDesc );
                    moveDesc.setAlignmentX( JCheckBox.LEFT_ALIGNMENT );
                    moveDesc.setBackground(dBlue);
                    moveDesc.setForeground(Color.WHITE);
                    moveDesc.setFont(ptSansRegular);
                    moveDesc.setLineWrap(true);
                    moveDesc.setWrapStyleWord(true);
                    moveDesc.setEditable( false );

                    moveEntryPanel.add( moveEntry, BorderLayout.WEST );
                    moveEntryPanel.add( moveDesc, BorderLayout.SOUTH );

                    moveEntries.add( moveEntryPanel );
                }

                moves.add( moveEntries );

                // Content for Special Mechanics

                ArrayList<SpecialMechanic> smList = currentArchetype.GetSpecialMechanics();
                specialMechanicsInput = new ArrayList<>();

                for( SpecialMechanic sm : smList ){
                    JTextArea mechanicLabel = new JTextArea( sm.GetMechanicName());
                    mechanicLabel.setEditable( false );
                    mechanicLabel.setLineWrap(true);
                    mechanicLabel.setWrapStyleWord(true);
                    moveLabel.setPreferredSize(new Dimension(600, 30));
                    mechanicLabel.setBackground(dBlue);
                    mechanicLabel.setForeground(Color.WHITE);
                    mechanicLabel.setFont(ptSansSmall);

                    moves.add( emptyLabel );
                    moves.add( mechanicLabel );

                    JTextArea mechanicDesc = new JTextArea( sm.GetMechanicDesc() );
                    mechanicDesc.setEditable( false );
                    mechanicDesc.setBackground(dBlue);
                    mechanicDesc.setForeground(Color.WHITE);
                    mechanicDesc.setFont(ptSansRegular);
                    mechanicDesc.setLineWrap(true);
                    mechanicDesc.setWrapStyleWord(true);
                    moves.add( mechanicDesc );

                    JTextArea mechanicSelection = new JTextArea();
                    mechanicSelection.setEditable( false );
                    mechanicSelection.setBackground(dBlue);
                    mechanicSelection.setForeground(Color.WHITE);
                    mechanicSelection.setFont(ptSansRegular);
                    mechanicSelection.setLineWrap(true);
                    mechanicSelection.setWrapStyleWord(true);

                    for ( SpecialMechanicSelection sms : sm.GetMechanicList() ){
                        if ( sms.IsJSONObject() ){
                            mechanicSelection.append( "  " + sms.GetName() + "\n" );
                        }

                        for ( String entry : sms.GetSelection() ){
                            mechanicSelection.append( "  -> " + entry + "\n" );
                        }
                        mechanicSelection.append( "\n" );

                    }

                    moves.add(mechanicSelection);

                    JTextArea mechanicInput = new JTextArea();
                    mechanicInput.setRows(5);
                    mechanicInput.setBackground(Color.WHITE);
                    mechanicInput.setFont(ptSansRegular);
                    mechanicInput.setLineWrap(true);
                    mechanicInput.setWrapStyleWord(true);

                    moves.add( mechanicInput );

                    specialMechanicsInput.add( mechanicInput );
                }
                
                revalidate();
                repaint();
            }
        }
        archetypeSelection.addActionListener(new getComboBoxInput());

        class SaveCharacter implements ActionListener {
            
            public void actionPerformed( ActionEvent ae ){
                String characterName = charName.getText();
                String characterDesc = descriptionText.getText();
                CharacterArchetype characterArchetype = new CharacterArchetype( currentArchetype.GetArchetypeName(), currentArchetype.GetArchetypeDesc() );

                ArrayList<Move> selectedMoves = new ArrayList<>();
                Character character = new Character( characterName, characterDesc, characterArchetype );

                for ( Move m : currentArchetype.GetMoveList().GetMoveList() ){
                    if ( m.IsPreselected() ){
                        selectedMoves.add( m );
                    }
                }

                for ( JCheckBox c : moveCheckList ){
                    if ( c.isSelected() ){
                        for ( Move m : currentArchetype.GetMoveList().GetMoveList() ){
                            if ( m.GetMoveName().equals( c.getText() ) ){
                                selectedMoves.add( m );
                                break;
                            }
                        }
                    }
                }

                characterArchetype.SetLuckSpecial( currentArchetype.GetLuckSpecial() );
                characterArchetype.SetMoves( selectedMoves );
                character.SetInventory( gearInput.getText() );

                int i = 0;
                for ( SpecialMechanic sm : currentArchetype.GetSpecialMechanics() ){
                    String specialMechanicName = sm.GetMechanicName();
                    String specialMechanicDesc = sm.GetMechanicDesc();
                    String specialMechanicEntry = specialMechanicsInput.get(i).getText();

                    CharacterSpecialMechanic csm = new CharacterSpecialMechanic( specialMechanicName, specialMechanicDesc, specialMechanicEntry);

                    characterArchetype.AddSpecialMechanicsToList( csm );

                    i += 1;
                }

                Rating selectedRating = new Rating();
                for ( Rating r : currentArchetype.GetRatings() ){
                    if ( r.GetRatingName().equals( ratingSelection.getText() ) ){
                        selectedRating = r;
                        break;
                    }
                }
                characterArchetype.SetRatings( selectedRating );

                

                try{
                    fl.WriteCharacterToFile( character );
                    Screen.removeAll();
                    LaunchScreenGUI launchScreen = new LaunchScreenGUI();
                    launchScreen.setSize(1440, 1024);
                    launchScreen.setTitle("Monster of the Week Hunter Companion");
                    launchScreen.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                    launchScreen.setVisible(true);
                    dispose();
                    revalidate();
                    repaint();
                }
                catch ( IOException ioe ){
                    ioe.printStackTrace();
                }
            }

        }

        saveCharacter = new JButton( "Save Character" );
        saveCharacter.setBackground(lBlue);
        saveCharacter.setForeground(Color.WHITE);
        saveCharacter.setFont(ptSansSmall);
        saveCharacter.addActionListener( new SaveCharacter() );

        savePanel.add( saveCharacter );
        contentPanel.add(topPanel);
        contentPanel.add(characterDescription);
        contentPanel.add(ratings);
        contentPanel.add(gear);
        contentPanel.add(moves);
        contentPanel.add( savePanel );


        contentPanelContainer.add(contentPanel);
        contentPanel.setBorder(BorderFactory.createLineBorder(Color.RED));

        Screen.setLayout(new FlowLayout());

        Screen.setBackground(dBlue);
        Screen.add(scrollPane);

    }
}
