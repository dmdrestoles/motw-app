/**
 * This is the Character class of the program.
 * @author Team DM
 * @version 0.1
 */

 public class Character{
	// FIELDS
	private String name;
	private String desc;
	private CharacterArchetype archetype;
	
	private int harm;
	private int luck;
	private int forward;
	private int experience;
	private int armor;
	private String inventory;

	// CONSTRUCTORS
	public Character(){
		name = "";
		desc = "";
		archetype = new CharacterArchetype();
		harm = 0;
		luck = 0;
		forward = 0;
		experience = 0;
		armor = 0;
		inventory = "";
	}

	public Character( String n, String d ){
		name = n;
		desc = d;
		archetype = new CharacterArchetype();
		harm = 0;
		luck = 0;
		forward = 0;
		experience = 0;
		armor = 0;
		inventory = "";
	}

	public Character( String n, String d, CharacterArchetype a ){
		name = n;
		desc = d;
		archetype = a;
		harm = 0;
		luck = 0;
		forward = 0;
		experience = 0;
		armor = 0;
		inventory = "";
	}

	 // METHODS
	public String GetCharacterName(){
		return name;
	}

	public String GetCharacterDesc(){
		return desc;
	}

	public CharacterArchetype GetCharacterArchetype(){
		return archetype;
	}

	public int GetHarm(){
		return harm;
	}

	public int GetLuck(){
		return luck;
	}

	public int GetForward(){
		return forward;
	}

	public int GetExperience(){
		return experience;
	}

	public int GetArmor(){
		return armor;
	}

	public String GetInventory(){
		return inventory;
	}

	public void SetCharacterName( String n ){
		name = n;
	}

	public void SetCharacterDesc( String d ){
		desc = d;
	}

	public void SetCharacterArchetype( CharacterArchetype a ){
		archetype = a;
	}

	public void SetHarm( int v ){
		harm = v;
	}

	public void SetLuck( int v ){
		luck = v;
	}

	public void SetForward( int v ){
		forward = v;
	}

	public void SetExperience( int v ){
		experience = v;
	}

	public void SetArmor( int v ){
		armor = v;
	}

	public void SetInventory( String i ){
		inventory = i;
	}
	
 }