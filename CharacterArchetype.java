/**
 * This is the Character Archetype class of the program.
 * @author Team DM
 * @version 0.1
 */

import java.util.*;
import java.io.*;

public class CharacterArchetype {
	
	// FIELDS
	private String name;
	private String desc;
	private String luckSpecial;

	private ArrayList<CharacterSpecialMechanic> specialMechanics;
	private ArrayList<Move> moves;
	private String gears;
	private Rating ratings;
	private String improvements;
	private String advancedImprovements;

	// CONSTRUCTORS
	public CharacterArchetype(){
		this.name = "";
		this.desc = "";
		this.luckSpecial = "";
		this.specialMechanics = new ArrayList<>();
		this.moves = new ArrayList<>();
		this.gears = "";
		this.ratings = new Rating();
		this.improvements = "";
		this.advancedImprovements = "";
	}

	public CharacterArchetype( String n, String d ){
		this.name = n;
		this.desc = d;
		this.luckSpecial = "";
		this.specialMechanics = new ArrayList<>();
		this.moves = new ArrayList<>();
		this.gears = "";
		this.ratings = new Rating();
		this.improvements = "";
		this.advancedImprovements = "";
	}

	public String GetName(){
		return this.name;
	}

	public String GetDesc(){
		return this.desc;
	}

	public String GetLuckSpecial(){
		return this.luckSpecial;
	}

	public ArrayList<CharacterSpecialMechanic> GetSpecialMechanics(){
		return this.specialMechanics;
	}

	public ArrayList<Move> GetMoves(){
		return this.moves;
	}

	public String GetGears(){
		return this.gears;
	}

	public Rating GetRatings(){
		return this.ratings;
	}

	public String GetImprovements(){
		return this.improvements;
	}

	public String GetAdvancedImprovements(){
		return this.advancedImprovements;
	}

	public void SetName( String n ){
		this.name = n;
	}

	public void SetDesc( String d ){
		this.desc = d;
	}

	public void SetLuckSpecial( String d ){
		this.luckSpecial = d;
	}

	public void SetSpecialMechanics( ArrayList<CharacterSpecialMechanic> objList ){
		this.specialMechanics = objList;
	}

	public void SetMoves( ArrayList<Move> objList ){
		this.moves = objList;
	}

	public void SetGears( String d ){
		this.gears = d;
	}

	public void SetRatings( Rating r ){
		this.ratings = r;
	}

	public void SetImprovements( String d ){
		this.improvements = d;
	}

	public void SetAdvancedImprovements( String d ){
		this.advancedImprovements = d;
	}

	public void AddSpecialMechanicsToList( CharacterSpecialMechanic obj ){
		this.specialMechanics.add( obj );
	}

	public void AddMoveToList( Move obj ){
		this.moves.add( obj );
	}

}
