/**
 * This is the Rating class of the program.
 * @author Team DM
 * @version 0.1
 */

import java.util.*;
import java.io.*;

public class Rating {
	
	// FIELDS
	private String name;
	private int charm, cool, sharp, tough, weird;

	// CONSTRUCTORS
	public Rating(){
		this.name = "";
		this.charm = 0;
		this.cool = 0;
		this.sharp = 0;
		this.tough = 0;
		this.weird = 0;
	}

	public Rating( String n ){
		this.name = n;
		this.charm = 0;
		this.cool = 0;
		this.sharp = 0;
		this.tough = 0;
		this.weird = 0;
	}

	public Rating( String n, int a, int b, int c, int d, int e ){
		this.name = n;
		this.charm = a;
		this.cool = b;
		this.sharp = c;
		this.tough = d;
		this.weird = e;
	}

	// METHODS
	public String GetRatingName(){
		return this.name;
	}

	public int GetCharm(){
		return this.charm;
	}

	public int GetCool(){
		return this.cool;
	}

	public int GetSharp(){
		return this.sharp;
	}

	public int GetTough(){
		return this.tough;
	}

	public int GetWeird(){
		return this.weird;
	}

	public void SetCharm( int c ){
		this.charm = c;
	}

	public void SetCool( int c ){
		this.cool = c;
	}

	public void SetSharp( int c ){
		this.sharp = c;
	}

	public void SetTough( int c ){
		this.tough = c;
	}

	public void SetWeird( int c ){
		this.weird = c;
	} 

}
