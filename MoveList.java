/**
 * This is the MoveList class of the program.
 * @author Team DM
 * @version 0.1
 */

import java.util.*;
import java.io.*;

public class MoveList {
	
	// FIELDS
	private String desc;
	private int maxMoveCount;
	private ArrayList<Move> moves;
	
	// CONSTRUCTORS
	public MoveList(){
		this.desc = "";
		this.maxMoveCount = 0;
		this.moves = new ArrayList<>();
	}

	public MoveList( String d ){
		this.desc = d;
		this.maxMoveCount = 0;
		this.moves = new ArrayList<>();
	}

	public MoveList( String d, int c ){
		this.desc = d;
		this.maxMoveCount = c;
		this.moves = new ArrayList<>();
	}

	// METHODS
	public String GetMovesDesc(){
		return this.desc;
	}

	public int GetMaxMoveCount(){
		return this.maxMoveCount;
	}

	public ArrayList<Move> GetMoveList(){
		return this.moves;
	}

	public void SetMovesDesc( String d ){
		this.desc = d;
	}

	public void SetMaxMoveCount( int c ){
		this.maxMoveCount = c;
	}

	public void AddMoveToList( Move m ){
		this.moves.add( m );
	}

}
