import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import javax.swing.*;
import java.io.*;
import java.util.*;
import javax.imageio.ImageIO;

public class DiceRollGUI extends JFrame
{
    public Container Screen;
    public Font statRolledFont, rollValueFont, breakdownTitleFont, breakdownStatFont, breakdownNumsFont;
    public JPanel centerPanel, leftPanel, rightPanel, leftSouthPanel, leftSouthWestPanel, leftSouthEastPanel;
    public String statRolledString;
    public Random randomNumber1, randomNumber2;
    public int dice1, dice2, statBuff, forwardBuff;

    public DiceRollGUI(String s, int i, int f)
    {
        Screen = this.getContentPane();
        
        switch(s)
        {
            case "Roll Charm":
            {
                statRolledString = "Charm";
                break;
            }
            case "Roll Cool":
            {
                statRolledString = "Cool";
                break;
            }
            case "Roll Sharp":
            {
                statRolledString = "Cool";
                break;
            }
            case "Roll Tough":
            {
                statRolledString = "Tough";
                break;
            }
            case "Roll Weird":
            {
                statRolledString = "Weird";
                break;
            }
        }

        randomNumber1 = new Random();
        dice1 = 1 + randomNumber1.nextInt((6 - 1) + 1);
        dice2 = 1 + randomNumber1.nextInt((6 - 1) + 1);

        statBuff = i;
        forwardBuff = f;

        System.out.println(dice1);
        System.out.println(dice2);

        DiceRollScreen();
    }
    
    public void DiceRollScreen()
    {
        Screen.setBackground(new Color(33, 35, 44));
        Screen.setLayout(new BorderLayout());
        
        centerPanel = new JPanel();
        centerPanel.setBackground(new Color(33, 35, 44));
        centerPanel.setLayout(new BorderLayout());
        Screen.add(centerPanel, BorderLayout.CENTER);
        
        leftPanel = new JPanel();
        leftPanel.setBackground(new Color(33, 35, 44));
        leftPanel.setLayout(new BorderLayout());
        Screen.add(leftPanel, BorderLayout.WEST);
        
        rightPanel = new JPanel();
        rightPanel.setBackground(new Color(33, 35, 44));
        Screen.add(rightPanel, BorderLayout.EAST);

        statRolledFont = new Font("Josefin Sans", Font.BOLD, 36);
        
        JLabel statRolled = new JLabel(" "){
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                g.drawString(("ROLL: " + statRolledString), 64, 64);
            }
        };
        statRolled.setFont(statRolledFont);
        statRolled.setPreferredSize(new Dimension(259, 65));
        statRolled.setForeground(Color.WHITE);
        
        Screen.add(statRolled, BorderLayout.NORTH);

        rollValueFont = new Font("PT Mono", Font.PLAIN, 96);
        
        JLabel rollValue = new JLabel(" "){
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                g.drawString(Integer.toString(dice1 + dice2 + statBuff + forwardBuff), 76, 140);
            }
        };
        rollValue.setFont(rollValueFont);
        rollValue.setPreferredSize(new Dimension(400, 108));
        rollValue.setForeground(Color.WHITE);
        
        centerPanel.add(rollValue, BorderLayout.WEST);
        
        breakdownTitleFont = new Font("Josefin Sans", Font.PLAIN, 24);
        
        JLabel breakdownTitle = new JLabel(" "){
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                g.drawString("BREAKDOWN", 64, 230);
            }
        };
        breakdownTitle.setFont(breakdownTitleFont);
        breakdownTitle.setPreferredSize(new Dimension(233, 200));
        breakdownTitle.setForeground(Color.WHITE);
        
        leftPanel.add(breakdownTitle, BorderLayout.CENTER);
        
        leftSouthPanel = new JPanel();
        leftSouthPanel.setBackground(new Color(33, 35, 44));
        leftSouthPanel.setLayout(new BorderLayout());
        leftPanel.add(leftSouthPanel, BorderLayout.SOUTH);
        
        leftSouthWestPanel = new JPanel();
        leftSouthWestPanel.setBackground(new Color(33, 35, 44));
        leftSouthWestPanel.setLayout(new BorderLayout());
        leftSouthPanel.add(leftSouthWestPanel, BorderLayout.WEST);
        
        breakdownStatFont = new Font("PT Sans", Font.PLAIN, 20);

        JLabel breakdownStat1 = new JLabel(" "){
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                g.drawString("Base Roll:", 64, 20);
            }
        };
        breakdownStat1.setFont(breakdownStatFont);
        breakdownStat1.setPreferredSize(new Dimension(160, 38));
        breakdownStat1.setForeground(Color.WHITE);
        leftSouthWestPanel.add(breakdownStat1, BorderLayout.NORTH);
        
        JLabel breakdownStat2 = new JLabel(" "){
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                g.drawString("Forward:", 64, 20);
            }
        };
        breakdownStat2.setFont(breakdownStatFont);
        breakdownStat2.setPreferredSize(new Dimension(160, 38));
        breakdownStat2.setForeground(Color.WHITE);
        leftSouthWestPanel.add(breakdownStat2, BorderLayout.CENTER);
        
        JLabel breakdownStat3 = new JLabel(" "){
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                g.drawString("Move:", 64, 20);
            }
        };
        breakdownStat3.setFont(breakdownStatFont);
        breakdownStat3.setPreferredSize(new Dimension(160, 90));
        breakdownStat3.setForeground(Color.WHITE);
        leftSouthWestPanel.add(breakdownStat3, BorderLayout.SOUTH);
        
        leftSouthEastPanel = new JPanel();
        leftSouthEastPanel.setBackground(new Color(33, 35, 44));
        leftSouthEastPanel.setLayout(new BorderLayout());
        leftSouthPanel.add(leftSouthEastPanel, BorderLayout.EAST);
        
        breakdownNumsFont = new Font("PT Mono", Font.BOLD, 20);
        
        JLabel breakdownNum1 = new JLabel(" "){
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                g.drawString(Integer.toString(dice1 + dice2), 55, 20);
            }
        };
        breakdownNum1.setFont(breakdownNumsFont);
        breakdownNum1.setPreferredSize(new Dimension(80, 38));
        breakdownNum1.setForeground(Color.WHITE);
        leftSouthEastPanel.add(breakdownNum1, BorderLayout.NORTH);
        
        JLabel breakdownNum2 = new JLabel(" "){
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                g.drawString(Integer.toString(forwardBuff), 44, 20);
            }
        };
        breakdownNum2.setFont(breakdownNumsFont);
        breakdownNum2.setPreferredSize(new Dimension(80, 38));
        breakdownNum2.setForeground(Color.WHITE);
        leftSouthEastPanel.add(breakdownNum2, BorderLayout.CENTER);
        
        JLabel breakdownNum3 = new JLabel(" "){
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                g.drawString(Integer.toString(statBuff), 44, 20);
            }
        };
        breakdownNum3.setFont(breakdownNumsFont);
        breakdownNum3.setPreferredSize(new Dimension(80, 90));
        breakdownNum3.setForeground(Color.WHITE);
        leftSouthEastPanel.add(breakdownNum3, BorderLayout.SOUTH);
    }
}
