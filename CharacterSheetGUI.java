import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import javax.swing.*;
import java.io.*;
import java.util.*;
import javax.imageio.ImageIO;

public class CharacterSheetGUI extends JFrame
{
    public Container Screen;
    public JPanel westPanel, westPanel1, westPanel2, centerPanel, eastPanel, charPanel, charPanelInner, 
                  harmPanel, harmPanelLeft, harmPanelLeftInner, harmRadioButtonContainer, harmPanelRight, 
                  statsPanel, charmPanel, coolPanel, sharpPanel, toughPanel, weirdPanel,
                  forwardPanel, luckPanel, luckPanelInner, luckCheckContainer, experiencePanel, experienceCheckContainer,
                  centerPanel1, centerPanel2, archetypeMovesPanel, basicMovesPanel;

    public JLabel charImage, charName, lineSeparator, charArchetype, 
                  harmPanelTag1, harmPanelTag2, harmPanelDesc1, harmPanelDesc2, okay, dying,
                  statCharm, statCool, statSharp, statTough, statWeird, descCharm, descCool, descSharp, descTough, descWeird,
                  forwardTag, forwardValue, luckTag, luckOkay, luckDoomed, experienceTag, experienceTag2;
    public JButton returnButton, rollCharm, rollCool, rollSharp, rollTough, rollWeird, minusButton, plusButton,
                   archetypeMovePlaceholder, basicMovePlaceholder;
    public JTextField charmValue, coolValue, sharpValue, toughValue, weirdValue;
    public JTextArea armorNumber, luckDesc, luckSpecial, experienceDesc, archetypeMovesLabel, basicMovesLabel;
    public JCheckBox harmCheck, unstableCheck, luckCheck, experienceCheck;
    public Color dViolet, lViolet;
    public Font cantarellS, ptSansSmall, ptSansSmallB, ptSansBig, ptSansItalic, ptSansSmallest, ptMono, Smythe, josefinSans;
    public int forwardInt;
    public DiceRollGUI diceRollPopup;
    public LaunchScreenGUI launchScreen;
    public FileLoader fl;
    public Character selectedCharacter;

    public ArrayList<JCheckBox> harmCheckboxList, luckCheckboxList, experienceCheckboxList; 

    public CharacterSheetGUI(String s)
    {
        dViolet = new Color(19, 11, 16);
        lViolet = new Color(48, 28, 41);
        cantarellS = new Font("Cantarell-Bold", Font.PLAIN, 20);
        ptSansBig = new Font("PT Sans", Font.PLAIN, 24);
        ptSansItalic = new Font("PT Sans", Font.ITALIC, 20);
        ptSansSmall = new Font("PT Sans", Font.PLAIN, 12);
        ptSansSmallB = new Font("PT Sans", Font.BOLD, 12);
        ptSansSmallest = new Font("PT Sans", Font.PLAIN, 10);
        ptMono = new Font("PT Mono", Font.BOLD, 50);
        Smythe = new Font("Smythe", Font.BOLD, 30);
        josefinSans = new Font("Josefin Sans", Font.PLAIN, 24);

        forwardInt = 0;

        fl = new FileLoader();
        try {	
			fl.VerifyCharacter();
            fl.VerifyCompendium();
		}
		catch (IOException ioe){
			ioe.printStackTrace();
		}

        selectedCharacter = new Character();
        harmCheckboxList = new ArrayList<>();
        luckCheckboxList = new ArrayList<>();
        experienceCheckboxList = new ArrayList<>();

        Screen = this.getContentPane();

        CharacterSheetScreen(Screen, s);
    }

    public void CharacterSheetScreen(Container Screen, String CharacterName)
    {
        for (Character c: fl.GetCharacters())
        {
            if (CharacterName.equals(c.GetCharacterName()))
            {
                selectedCharacter = c;
            }
        }

        westPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 1, 0));
        westPanel.setBackground(Color.WHITE);
        westPanel1 = new JPanel(new FlowLayout(FlowLayout.LEFT, 25, 10));
        westPanel1.setBackground(lViolet);
        westPanel1.setPreferredSize(new Dimension(360, 1024));
        westPanel2 = new JPanel(new FlowLayout(FlowLayout.LEFT, 25, 30));
        westPanel2.setBackground(lViolet);
        westPanel2.setPreferredSize(new Dimension(250, 1024));
        centerPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 25, 40));
        centerPanel.setBackground(dViolet);
        centerPanel.setMaximumSize(new Dimension(464, 1024));
        eastPanel = new JPanel(new BorderLayout());
        eastPanel.setBackground(lViolet);
        eastPanel.setPreferredSize(new Dimension(300, 1024));


        //WEST PANEL STUFF

        //CHAR PANEL STUFF
        charPanel = new JPanel(new FlowLayout());
        charPanel.setBackground(lViolet);
        charPanelInner = new JPanel();
        charPanelInner.setLayout(new BoxLayout(charPanelInner, BoxLayout.PAGE_AXIS));
        charPanelInner.setBackground(lViolet);

        charImage = new JLabel("Place Image Here", JLabel.CENTER);
        charImage.setPreferredSize(new Dimension(130,130));
        charImage.setOpaque(true);
        charImage.setBackground(Color.WHITE);
        charImage.setForeground(Color.BLACK);

        charName = new JLabel(selectedCharacter.GetCharacterName());
        charName.setBackground(lViolet);
        charName.setForeground(Color.WHITE);
        charName.setOpaque(true);
        charName.setFont(ptSansBig);

        lineSeparator = new JLabel();
        lineSeparator.setPreferredSize(new Dimension(200, 5));
        lineSeparator.setBackground(Color.WHITE);
        lineSeparator.setOpaque(true);

        charArchetype = new JLabel(selectedCharacter.GetCharacterArchetype().GetName());
        charArchetype.setBackground(lViolet);
        charArchetype.setForeground(Color.WHITE);
        charArchetype.setOpaque(true);
        charArchetype.setFont(ptSansItalic);

        charPanel.add(charImage);
        charPanelInner.add(charName);
        charPanelInner.add(lineSeparator);
        charPanelInner.add(charArchetype);
        charPanel.add(charPanelInner);
        
        //HARM PANEL STUFF
        harmPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 10, 10));
        harmPanel.setBackground(lViolet);

        harmPanelLeft = new JPanel(new BorderLayout(20, 10));
        harmPanelLeft.setBackground(lViolet);
        harmPanelLeft.setBorder(BorderFactory.createLineBorder(Color.WHITE));

        harmPanelLeftInner = new JPanel(new BorderLayout(0, 10));
        harmPanelLeftInner.setBackground(lViolet);

        harmPanelRight = new JPanel(new BorderLayout());
        harmPanelRight.setBackground(lViolet);

        harmPanelTag1 = new JLabel("   Harm");
        harmPanelTag1.setFont(ptSansSmallB);
        harmPanelTag1.setBackground(lViolet);
        harmPanelTag1.setForeground(Color.WHITE);
        harmPanelTag1.setOpaque(true);

        harmPanelTag2 = new JLabel("Armor");
        harmPanelTag2.setFont(ptSansSmallB);
        harmPanelTag2.setBackground(lViolet);
        harmPanelTag2.setForeground(Color.WHITE);
        harmPanelTag2.setOpaque(true);

        armorNumber = new JTextArea(Integer.toString(selectedCharacter.GetArmor()));
        armorNumber.setFont(ptMono);
        armorNumber.setBackground(lViolet);
        armorNumber.setForeground(Color.WHITE);
        armorNumber.setOpaque(true);

        harmPanelDesc1 = new JLabel("   When you reach 4 or more, mark unstable");
        harmPanelDesc1.setFont(ptSansSmall);
        harmPanelDesc1.setBackground(lViolet);
        harmPanelDesc1.setForeground(Color.WHITE);
        harmPanelDesc1.setOpaque(true);

        okay = new JLabel("   Okay");
        okay.setBackground(lViolet);
        okay.setForeground(Color.WHITE);
        okay.setFont(ptSansSmall);
        dying = new JLabel("Dying   ");
        dying.setBackground(lViolet);
        dying.setForeground(Color.WHITE);
        dying.setFont(ptSansSmall);

        harmRadioButtonContainer = new JPanel(new FlowLayout());
        harmRadioButtonContainer.setBackground(lViolet);

        int characterHarm = selectedCharacter.GetHarm();

        for (int i = 0; i < 7; i++)
        {
            harmCheck = new JCheckBox();
            harmCheck.setBackground(lViolet);
            harmCheck.setForeground(Color.WHITE);
            harmRadioButtonContainer.add(harmCheck);
            harmCheckboxList.add( harmCheck );

            if ( characterHarm > 0 ){
                harmCheck.setSelected( true );
                characterHarm--;
            }
        }

        unstableCheck = new JCheckBox("   Unstable");
        unstableCheck.setBackground(lViolet);
        unstableCheck.setForeground(Color.WHITE);

        harmPanelLeftInner.add(harmPanelDesc1, BorderLayout.NORTH);
        harmPanelLeftInner.add(harmRadioButtonContainer, BorderLayout.CENTER);
        harmPanelLeftInner.add(okay, BorderLayout.WEST);
        harmPanelLeftInner.add(dying, BorderLayout.EAST);
        harmPanelLeftInner.add(unstableCheck, BorderLayout.SOUTH);
        
        harmPanelLeft.add(harmPanelTag1, BorderLayout.NORTH);
        harmPanelLeft.add(harmPanelLeftInner, BorderLayout.CENTER);

        harmPanelRight.add(harmPanelTag2, BorderLayout.NORTH);
        harmPanelRight.add(armorNumber, BorderLayout.CENTER);

        harmPanel.add(harmPanelLeft);
        harmPanel.add(harmPanelRight);

        //STATS PANEL STUFF
        statsPanel = new JPanel();
        statsPanel.setLayout(new BoxLayout(statsPanel, BoxLayout.Y_AXIS));
        statsPanel.setBackground(lViolet);
        statsPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
        charmPanel = new JPanel(new FlowLayout());
        charmPanel.setBackground(lViolet);
        charmPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
        coolPanel = new JPanel(new FlowLayout());
        coolPanel.setBackground(lViolet);
        coolPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
        sharpPanel = new JPanel(new FlowLayout());
        sharpPanel.setBackground(lViolet);
        sharpPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
        toughPanel = new JPanel(new FlowLayout());
        toughPanel.setBackground(lViolet);
        toughPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
        weirdPanel = new JPanel(new FlowLayout());
        weirdPanel.setBackground(lViolet);
        weirdPanel.setAlignmentX(Component.LEFT_ALIGNMENT);

        statCharm = new JLabel("Charm");
        statCharm.setBackground(lViolet);
        statCharm.setForeground(Color.WHITE);
        statCharm.setFont(Smythe);
        statCharm.setPreferredSize(new Dimension(100, 50));

        statCool = new JLabel("Cool");
        statCool.setBackground(lViolet);
        statCool.setForeground(Color.WHITE);
        statCool.setFont(Smythe);
        statCool.setPreferredSize(new Dimension(100, 50));

        statSharp = new JLabel("Sharp");
        statSharp.setBackground(lViolet);
        statSharp.setForeground(Color.WHITE);
        statSharp.setFont(Smythe);
        statSharp.setPreferredSize(new Dimension(100, 50));

        statTough = new JLabel("Tough");
        statTough.setBackground(lViolet);
        statTough.setForeground(Color.WHITE);
        statTough.setFont(Smythe);
        statTough.setPreferredSize(new Dimension(100, 50));

        statWeird = new JLabel("Weird");
        statWeird.setBackground(lViolet);
        statWeird.setForeground(Color.WHITE);
        statWeird.setFont(Smythe);
        statWeird.setPreferredSize(new Dimension(100, 50));

        charmValue = new JTextField(Integer.toString(selectedCharacter.GetCharacterArchetype().GetRatings().GetCharm()));
        charmValue.setPreferredSize(new Dimension(25, 25));
        charmValue.setEditable(true);
        coolValue = new JTextField(Integer.toString(selectedCharacter.GetCharacterArchetype().GetRatings().GetCool()));
        coolValue.setPreferredSize(new Dimension(25, 25));
        coolValue.setEditable(true);
        sharpValue = new JTextField(Integer.toString(selectedCharacter.GetCharacterArchetype().GetRatings().GetSharp()));
        sharpValue.setPreferredSize(new Dimension(25, 25));
        sharpValue.setEditable(true);
        toughValue = new JTextField(Integer.toString(selectedCharacter.GetCharacterArchetype().GetRatings().GetTough()));
        toughValue.setPreferredSize(new Dimension(25, 25));
        toughValue.setEditable(true);
        weirdValue = new JTextField(Integer.toString(selectedCharacter.GetCharacterArchetype().GetRatings().GetWeird()));
        weirdValue.setPreferredSize(new Dimension(25, 25));
        weirdValue.setEditable(true);

        rollCharm = new JButton("Roll Charm");
        rollCharm.setPreferredSize(new Dimension(90, 30));
        rollCharm.setFont(ptSansSmallest);
        rollCharm.setBackground(dViolet);
        rollCharm.setForeground(Color.WHITE);

        rollCool = new JButton("Roll Cool");
        rollCool.setPreferredSize(new Dimension(90, 30));
        rollCool.setFont(ptSansSmallest);
        rollCool.setBackground(dViolet);
        rollCool.setForeground(Color.WHITE);

        rollSharp = new JButton("Roll Sharp");
        rollSharp.setFont(ptSansSmallest);
        rollSharp.setPreferredSize(new Dimension(90, 30));
        rollSharp.setBackground(dViolet);
        rollSharp.setForeground(Color.WHITE);

        rollTough = new JButton("Roll Tough");
        rollTough.setPreferredSize(new Dimension(90, 30));
        rollTough.setFont(ptSansSmallest);
        rollTough.setBackground(dViolet);
        rollTough.setForeground(Color.WHITE);

        rollWeird = new JButton("Roll Weird");
        rollWeird.setPreferredSize(new Dimension(90, 30));
        rollWeird.setFont(ptSansSmallest);
        rollWeird.setBackground(dViolet);
        rollWeird.setForeground(Color.WHITE);

        charmPanel.add(rollCharm);
        charmPanel.add(statCharm);
        charmPanel.add(charmValue);

        coolPanel.add(rollCool);
        coolPanel.add(statCool);
        coolPanel.add(coolValue);

        sharpPanel.add(rollSharp);
        sharpPanel.add(statSharp);
        sharpPanel.add(sharpValue);

        toughPanel.add(rollTough);
        toughPanel.add(statTough);
        toughPanel.add(toughValue);

        weirdPanel.add(rollWeird);
        weirdPanel.add(statWeird);
        weirdPanel.add(weirdValue);

        statsPanel.add(charmPanel);
        statsPanel.add(coolPanel);
        statsPanel.add(sharpPanel);
        statsPanel.add(toughPanel);
        statsPanel.add(weirdPanel);
        
        returnButton = new JButton("<<< Return to Character Select");
        returnButton.setBackground(lViolet);
        returnButton.setForeground(Color.WHITE);
        returnButton.setFont(cantarellS);

        //FORWARD PANEL STUFF

        forwardPanel = new JPanel(new FlowLayout());
        forwardPanel.setBackground(lViolet);

        forwardTag = new JLabel("Forward / Ongoing: ");
        forwardTag.setFont(ptSansSmallB);
        forwardTag.setBackground(lViolet);
        forwardTag.setForeground(Color.WHITE);

        forwardValue = new JLabel(Integer.toString( selectedCharacter.GetForward() ));
        forwardValue.setBackground(lViolet);
        forwardValue.setForeground(Color.WHITE);
        forwardValue.setFont(ptMono);

        minusButton = new JButton("-");
        minusButton.setBackground(dViolet);
        minusButton.setForeground(Color.WHITE);

        plusButton = new JButton("+");
        plusButton.setBackground(dViolet);
        plusButton.setForeground(Color.WHITE);

        forwardPanel.add(forwardTag);
        forwardPanel.add(minusButton);
        forwardPanel.add(forwardValue);
        forwardPanel.add(plusButton);

        // LUCK PANEL STUFF
        luckPanel = new JPanel(new BorderLayout());
        luckPanel.setBackground(lViolet);
        
        luckPanelInner = new JPanel(new BorderLayout());
        luckPanelInner.setBackground(lViolet);

        luckTag = new JLabel("Luck");
        luckTag.setFont(ptSansSmallB);
        luckTag.setBackground(lViolet);
        luckTag.setForeground(Color.WHITE);

        luckPanel.add(luckTag, BorderLayout.NORTH);
        luckPanel.add(luckPanelInner, BorderLayout.CENTER);

        luckDesc = new JTextArea("Mark luck to change a roll to 12 or avoid all harm from an injury");
        luckDesc.setBackground(lViolet);
        luckDesc.setForeground(Color.WHITE);
        luckDesc.setEditable( false );
        luckDesc.setFont(ptSansSmall);
        luckDesc.setLineWrap(true);
        luckDesc.setWrapStyleWord(true);
        

        luckCheckContainer = new JPanel(new FlowLayout());
        luckCheckContainer.setBackground(lViolet);
        luckCheckContainer.setPreferredSize(new Dimension(200, 25));

        int characterLuck = selectedCharacter.GetLuck();
        for (int i = 0; i < 7; i++)
        {
            luckCheck = new JCheckBox();
            luckCheck.setBackground(lViolet);
            luckCheck.setForeground(Color.WHITE);
            luckCheckContainer.add(luckCheck);
            luckCheckboxList.add( luckCheck );

            if ( characterLuck > 0 ){
                luckCheck.setSelected( true );
                characterLuck--;
            }
        }

        luckSpecial = new JTextArea("Luck Special: " + selectedCharacter.GetCharacterArchetype().GetLuckSpecial());
        luckSpecial.setBackground(lViolet);
        luckSpecial.setForeground(Color.WHITE);
        luckSpecial.setEditable( false );
        luckSpecial.setFont(ptSansSmall);
        luckSpecial.setLineWrap(true);
        luckSpecial.setWrapStyleWord(true);

        luckPanelInner.add(luckDesc, BorderLayout.NORTH);
        luckPanelInner.add(luckCheckContainer, BorderLayout.CENTER);
        luckPanelInner.add(luckSpecial, BorderLayout.SOUTH);

        // EXPERIENCE PANEL STUFF
        experiencePanel = new JPanel(new BorderLayout());
        experiencePanel.setBackground(lViolet);

        experienceTag = new JLabel("Experience");
        experienceTag.setFont(ptSansSmallB);
        experienceTag.setBackground(lViolet);
        experienceTag.setForeground(Color.WHITE);

        experienceTag2 = new JLabel("EXP: ");
        experienceTag2.setFont(ptSansSmall);
        experienceTag2.setBackground(lViolet);
        experienceTag2.setForeground(Color.WHITE);

        experienceCheckContainer = new JPanel(new FlowLayout(FlowLayout.CENTER, 0, 0));
        experienceCheckContainer.setBackground(lViolet);
        experienceCheckContainer.setPreferredSize(new Dimension(200, 25));

        experienceCheckContainer.add(experienceTag2);

        int characterExp = selectedCharacter.GetExperience() % 5;
        for (int i = 0; i < 5; i++)
        {
            experienceCheck = new JCheckBox();
            experienceCheck.setBackground(lViolet);
            experienceCheck.setForeground(Color.WHITE);
            experienceCheckContainer.add(experienceCheck);
            experienceCheckboxList.add( experienceCheck );

            if ( characterExp > 0 ){
                experienceCheck.setSelected( true );
                characterExp--;
            }
        }

        experienceDesc = new JTextArea("Whenever you roll and get a total of 6 or less, or when a move tells you to, mark an experience box");
        experienceDesc.setBackground(lViolet);
        experienceDesc.setForeground(Color.WHITE);
        experienceDesc.setEditable( false );
        experienceDesc.setFont(ptSansSmall);
        experienceDesc.setLineWrap(true);
        experienceDesc.setWrapStyleWord(true);

        experiencePanel.add(experienceTag, BorderLayout.NORTH);
        experiencePanel.add(experienceCheckContainer, BorderLayout.CENTER);
        experiencePanel.add(experienceDesc, BorderLayout.SOUTH);

        //CENTER PANEL STUFF:
        //The Center Panel Includes Archetype Specific Moves and Basic Moves
        //Please refer to the figma for this
        centerPanel1 = new JPanel();
        centerPanel1.setLayout( new BoxLayout( centerPanel1, BoxLayout.Y_AXIS ) );
        centerPanel1.setBackground(dViolet);

        centerPanel2 = new JPanel();
        centerPanel2.setLayout( new BoxLayout( centerPanel2, BoxLayout.Y_AXIS ) );
        centerPanel2.setBackground(dViolet);
        
        archetypeMovesLabel = new JTextArea("ARCHETYPE MOVES");
        archetypeMovesLabel.setAlignmentX( Component.LEFT_ALIGNMENT );
        archetypeMovesLabel.setEditable( false );
        archetypeMovesLabel.setAlignmentX( Component.LEFT_ALIGNMENT );
        archetypeMovesLabel.setBackground(dViolet);
        archetypeMovesLabel.setForeground(Color.WHITE);
        archetypeMovesLabel.setFont(josefinSans);

        JPanel archetypeMovesLabelPanel = new JPanel( new BorderLayout() );
        archetypeMovesLabelPanel.add( archetypeMovesLabel, BorderLayout.WEST );
        archetypeMovesLabelPanel.setBackground(dViolet);
        archetypeMovesLabelPanel.setForeground(Color.WHITE);

        basicMovesLabel = new JTextArea("BASIC MOVES");
        basicMovesLabel.setLineWrap( true );
        basicMovesLabel.setWrapStyleWord( true );
        basicMovesLabel.setEditable( false );
        basicMovesLabel.setBackground(dViolet);
        basicMovesLabel.setForeground(Color.WHITE);
        basicMovesLabel.setFont(josefinSans);
        
        archetypeMovesPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 28, 24));
        archetypeMovesPanel.setBackground(dViolet);

        basicMovesPanel = new JPanel( new GridLayout( 3, 3, 20, 20 ) );
        basicMovesPanel.setBackground(dViolet);

        for ( Move m : selectedCharacter.GetCharacterArchetype().GetMoves() ){
            JPanel moveEntry = new JPanel( new BorderLayout() );

            JTextArea moveEntryName = new JTextArea( m.GetMoveName() );
            moveEntryName.setLineWrap( true );
            moveEntryName.setWrapStyleWord( true );
            moveEntryName.setEditable( false );
            moveEntryName.setBackground( new Color(27,16,21) );
            moveEntryName.setForeground(Color.WHITE);
            moveEntryName.setFont(ptSansSmallB);

            JTextArea moveEntryDesc = new JTextArea( m.GetMoveDesc() );
            moveEntryDesc.setLineWrap( true );
            moveEntryDesc.setWrapStyleWord( true );
            moveEntryDesc.setEditable( false );
            moveEntryDesc.setBackground( lViolet );
            moveEntryDesc.setForeground(Color.WHITE);
            moveEntryDesc.setFont(ptSansSmall);

            moveEntry.add( moveEntryName, BorderLayout.NORTH );
            moveEntry.add( moveEntryDesc, BorderLayout.CENTER );
            moveEntry.setPreferredSize(new Dimension(170, 148));
            archetypeMovesPanel.add(moveEntry);
        }

        for ( BasicMove m : fl.GetBasicMoves() ){
            JPanel moveEntry = new JPanel( new BorderLayout() );

            JTextArea moveEntryName = new JTextArea( m.name );
            moveEntryName.setLineWrap( true );
            moveEntryName.setWrapStyleWord( true );
            moveEntryName.setEditable( false );
            moveEntryName.setBackground( new Color(27,16,21) );
            moveEntryName.setForeground(Color.WHITE);
            moveEntryName.setFont(ptSansSmallB);

            JTextArea moveEntryDesc = new JTextArea( m.desc );
            moveEntryDesc.setLineWrap( true );
            moveEntryDesc.setWrapStyleWord( true );
            moveEntryDesc.setEditable( false );
            moveEntryDesc.setBackground( lViolet );
            moveEntryDesc.setForeground(Color.WHITE);
            moveEntryDesc.setFont(ptSansSmall);

            moveEntry.add( moveEntryName, BorderLayout.NORTH );
            moveEntry.add( moveEntryDesc, BorderLayout.CENTER );
            moveEntry.setPreferredSize(new Dimension(170, 148));
            basicMovesPanel.add(moveEntry);
        }

        //EAST PANEL STUFF
        JPanel specialMechanicsPanel = new JPanel();
        specialMechanicsPanel.setLayout( new BoxLayout( specialMechanicsPanel, BoxLayout.Y_AXIS ) );

        for( CharacterSpecialMechanic csm : selectedCharacter.GetCharacterArchetype().GetSpecialMechanics() ){
            JPanel specialMechanicPanel = new JPanel( new BorderLayout() );
            specialMechanicPanel.setBackground( lViolet );

            JTextArea specialMechanicLabel = new JTextArea( csm.GetMechanicName() );
            specialMechanicLabel.setLineWrap( true );
            specialMechanicLabel.setWrapStyleWord( true );
            specialMechanicLabel.setEditable( false );
            specialMechanicLabel.setBackground( lViolet );
            specialMechanicLabel.setForeground(Color.WHITE);
            specialMechanicLabel.setFont(josefinSans);

            JTextArea specialMechanicEntryDesc = new JTextArea( csm.GetSpecialMechanicEntry() );
            specialMechanicEntryDesc.setLineWrap( true );
            specialMechanicEntryDesc.setWrapStyleWord( true );
            specialMechanicEntryDesc.setEditable( false );
            specialMechanicEntryDesc.setBackground( dViolet );
            specialMechanicEntryDesc.setForeground(Color.WHITE);
            specialMechanicEntryDesc.setFont(ptSansSmall);
            specialMechanicEntryDesc.setPreferredSize( new Dimension( 240, 130 ) );

            specialMechanicPanel.add( specialMechanicLabel, BorderLayout.NORTH );
            specialMechanicPanel.add( specialMechanicEntryDesc, BorderLayout.CENTER );

            specialMechanicsPanel.add( specialMechanicPanel, BorderLayout.CENTER );
        }

        JPanel inventoryPanel = new JPanel( new BorderLayout() );
        inventoryPanel.setBackground( lViolet );
        inventoryPanel.setForeground( Color.WHITE );

        JTextArea inventoryLabel = new JTextArea( "INVENTORY" );
        inventoryLabel.setLineWrap( true );
        inventoryLabel.setWrapStyleWord( true );
        inventoryLabel.setEditable( false );
        inventoryLabel.setBackground( lViolet );
        inventoryLabel.setForeground(Color.WHITE);
        inventoryLabel.setFont(josefinSans);

        JTextArea inventoryText = new JTextArea( selectedCharacter.GetInventory() );
        System.out.println( selectedCharacter.GetInventory() );
        inventoryText.setLineWrap( true );
        inventoryText.setWrapStyleWord( true );
        inventoryText.setFont(ptSansSmall);

        inventoryPanel.add( inventoryLabel, BorderLayout.NORTH );
        inventoryPanel.add( inventoryText, BorderLayout.CENTER );

        eastPanel.add( specialMechanicsPanel, BorderLayout.CENTER );
        eastPanel.add( inventoryPanel, BorderLayout.SOUTH );

        //ADDING PANELS TO THE SCREEN

        westPanel1.add(returnButton);
        westPanel1.add(charPanel);
        westPanel1.add(harmPanel);
        westPanel1.add(statsPanel);
        westPanel1.add(forwardPanel);

        westPanel2.add(luckPanel);
        westPanel2.add(experiencePanel);

        westPanel.add(westPanel1);
        westPanel.add(westPanel2);

        centerPanel1.add(archetypeMovesLabelPanel);
        centerPanel1.add(archetypeMovesPanel);

        centerPanel2.add(basicMovesLabel);
        centerPanel2.add(basicMovesPanel);

        centerPanel.add(centerPanel1);
        centerPanel.add(centerPanel2);

        Screen.setLayout(new BorderLayout());
        Screen.setBackground(Color.WHITE);

        Screen.add(westPanel, BorderLayout.WEST);
        Screen.add(centerPanel, BorderLayout.CENTER);
        Screen.add(eastPanel, BorderLayout.EAST);

        class DiceRollButton implements ActionListener
        {
            public void actionPerformed(ActionEvent ae)
            {   
                //Add the code so that the DiceRollGUI loads as a popup Frame
                //Something Like This:
                Object source = ae.getSource();
                JButton button = (JButton) source;
                int statBuff = 0;
                switch(button.getText())
                {
                    case "Roll Charm":
                    {
                        statBuff = Integer.parseInt(charmValue.getText());
                        break;
                    }
                    case "Roll Cool":
                    {
                        statBuff = Integer.parseInt(coolValue.getText());
                        break;
                    }
                    case "Roll Sharp":
                    {
                        statBuff = Integer.parseInt(sharpValue.getText());
                        break;
                    }
                    case "Roll Tough":
                    {
                        statBuff = Integer.parseInt(toughValue.getText());
                        break;
                    }
                    case "Roll Weird":
                    {
                        statBuff = Integer.parseInt(weirdValue.getText());
                        break;
                    }
                }
                diceRollPopup = new DiceRollGUI(button.getText(), statBuff, forwardInt);
                diceRollPopup.setSize(737, 514);
                diceRollPopup.setTitle("Monster of the Week Hunter Companion");
                diceRollPopup.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
                diceRollPopup.setVisible(true);
            }
        }
        rollCharm.addActionListener(new DiceRollButton());
        rollCool.addActionListener(new DiceRollButton());
        rollSharp.addActionListener(new DiceRollButton());
        rollTough.addActionListener(new DiceRollButton());
        rollWeird.addActionListener(new DiceRollButton());

        class UpdateForwardButton implements ActionListener
        {
            public void actionPerformed(ActionEvent ae)
            {
                forwardPanel.removeAll();

                Object source = ae.getSource();
                JButton button = (JButton) source;
                if(button.getText().equals("-"))
                {
                    forwardInt -= 1;
                } 
                else if(button.getText().equals("+"))
                {
                    forwardInt += 1;
                }

                selectedCharacter.SetForward( forwardInt );
                forwardValue = new JLabel(Integer.toString(forwardInt));
                forwardValue.setBackground(lViolet);
                forwardValue.setForeground(Color.WHITE);
                forwardValue.setFont(ptMono);

                forwardPanel.add(forwardTag);
                forwardPanel.add(minusButton);
                forwardPanel.add(forwardValue);
                forwardPanel.add(plusButton);
                revalidate();
                repaint();
            }
        }
        minusButton.addActionListener(new UpdateForwardButton());
        plusButton.addActionListener(new UpdateForwardButton());

        class ReturnButton implements ActionListener
        {
            public void actionPerformed(ActionEvent ae)
            {
                // SAVING THE CHARACTER SHEET'S STATE
                // Set Harm
                int currHarm = 0;

                for ( JCheckBox cb : harmCheckboxList ){
                    if ( cb.isSelected() ){
                        currHarm++;
                        continue;
                    }
                }

                selectedCharacter.SetHarm( currHarm );

                // Set Stats
                selectedCharacter.GetCharacterArchetype().GetRatings().SetCharm( Integer.parseInt( charmValue.getText() ) );
                selectedCharacter.GetCharacterArchetype().GetRatings().SetCool( Integer.parseInt( coolValue.getText() ) );
                selectedCharacter.GetCharacterArchetype().GetRatings().SetSharp( Integer.parseInt( sharpValue.getText() ) );
                selectedCharacter.GetCharacterArchetype().GetRatings().SetTough( Integer.parseInt( toughValue.getText() ) );
                selectedCharacter.GetCharacterArchetype().GetRatings().SetWeird ( Integer.parseInt( weirdValue.getText() ) );
                selectedCharacter.SetArmor( Integer.parseInt( armorNumber.getText() ) );
                selectedCharacter.SetForward( Integer.parseInt( forwardValue.getText() ) );

                // Set Luck
                int currLuck = 0;

                for ( JCheckBox cb : luckCheckboxList ){
                    if ( cb.isSelected() ){
                        currLuck++;
                        continue;
                    }
                }

                selectedCharacter.SetLuck( currLuck );

                // Set Experience
                int currExp = selectedCharacter.GetExperience();

                for ( JCheckBox cb : experienceCheckboxList ){
                    if ( cb.isSelected() ){
                        currExp++;
                        continue;
                    }
                }

                selectedCharacter.SetExperience( currExp );

                // Set Inventory
                selectedCharacter.SetInventory( inventoryText.getText() );

                try {
                    fl.WriteCharacterToFile( selectedCharacter );
                }
                catch ( IOException ioe ){
                    ioe.printStackTrace();
                }

                launchScreen = new LaunchScreenGUI();
                launchScreen.setSize(1440, 1024);
                launchScreen.setTitle("Monster of the Week Hunter Companion");
                launchScreen.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                launchScreen.setVisible(true);
                dispose();
                revalidate();
                repaint();
            }
        }
        returnButton.addActionListener(new ReturnButton());
    }
}
