{
	"name": "The Expert",
	"desc": "I have dedicated my life to the study of the unnatural. I know their habits, their weaknesses. I may not be youngest or strongest, but my knowledge makes me the biggest threat.",
	"luckSpecial": "When you spend a point of Luck, you discover something happening now is related to something you were involved in years ago.",

	"specialMechanics": [
		{
			"name": "Haven",
			"desc": "You have set up a haven, a safe place to work. Pick three of the options below for your haven:",
			"specialMechanicList": [
				"Lore Library. When you hit the books, take +1 forward to investigate the mystery (as long as historical or reference works are appropriate).",
				"Mystical Library - If you use your library's occult tomes and grimoires, preparing with your tomes and grimoires, take +1 forward for use magic.",
				"Protection Spells - Your haven is safe from monsters—they cannot enter. Monsters might be able to do something special to evade the wards, but not easily.",
				"Armory - You have a stockpile of mystical and rare monster-killing weapons and items. If you need a special weapon, roll +Weird. On a 10+ you have it (and plenty if that matters). On a 7-9 you have it, but only the minimum. On a miss, you've got the wrong thing.",
				"Infirmary - You can heal people, and have the space for one or two to recuperate. The Keeper will tell you how long any patient's recovery is likely to take, and if you need extra supplies or help.",
				"Workshop - You have a space for building and repairing guns, cars and other gadgets. Work out with the Keeper how long any repair or construction will take, and if you need extra supplies or help. ",
				"Oubliette - This room is isolated from every kind of monster, spirit and magic that you know about. Anything you stash in there can't be found, can't do any magic, and can't get out.",
				"Panic Room - This has essential supplies and is protected by normal and mystical means. You can hide out there for a few days, safe from pretty much anything.",
				"Magical Laboratory - You have a mystical lab with all kinds of weird ingredients and tools useful for casting spells (like the use magic move, big magic, and any other magical moves)."
			]
		}
	],

	"moves": {
		"desc": "Pick two moves.",
		"moveCount": 2,
		"moveList": [
			{
				"name": "I've Read About This Sort Of Thing (Sharp)",
				"isPreselected": false,
				"desc": "Roll +Sharp instead of +Cool when you act under pressure."
			},
			{
				"name": "Often Right",
				"isPreselected": false,
				"desc": "When a hunter comes to you for advice about a problem, give them your honest opinion and advice. If they take your advice, they get +1 ongoing while following your advice, and you mark experience."
			},
			{
				"name": "Preparedness (Sharp)",
				"isPreselected": false,
				"desc": "When you need something unusual or rare, roll +Sharp. On a 10+, you have it here right now. On a 7-9 you have it, but not here: it will take some time to get it. On a miss, you know where it is, but it's somewhere real bad."
			},
			{
				"name": "It Wasn't As Bad As It Looked (Cool)",
				"isPreselected": false,
				"desc": "Once per mystery, you may attempt to keep going despite your injuries. Roll +Cool. On a 10+, heal 2 harm and stabilize your wounds. On a 7-9 you may either stabilize or heal 1 harm. On a miss, it was worse than it looked: the Keeper may inflict a harm move on you, or make your wounds unstable."
			},
			{
				"name": "Precise Strike (Tough)",
				"isPreselected": false,
				"desc": "When you inflict harm on a monster, you can aim for a weak spot. Roll +Tough. On a 10+ you inflict +2 harm. On a 7-9 you inflict +1 harm. On a miss, you leave yourself open to the monster."
			},
			{
				"name": "The Woman (or Man) With The Plan (Sharp)",
				"isPreselected": false,
				"desc": "At the beginning of each mystery, roll +Sharp. On a 10+ hold 2, on a 7-9 hold 1. Spend the hold to be where you need to be, prepared and ready. On a miss, the Keeper holds 1 they can spend to put you in the worst place, unprepared and unready."
			},
			{
				"name": "Dark Past (Weird)",
				"isPreselected": false,
				"desc": "If you trawl through your memories for something relevant to the case at hand, roll +Weird. On a 10+ ask the Keeper two questions from the list below. On a 7-9 ask one. On a miss, you can ask a question anyway but that will mean you were personally complicit in creating the situation you are now dealing with. The questions are:\nWhen I dealt with this creature (or one of its kind), what did I learn?\nWhat black magic do I know that could help here?\nDo I know anyone who might be behind this?\nWho do I know who can help us right now?"
			}
		]
	},

	"gears": {
		"desc": "You get to pick your monster-slaying weapons.",
		"gearSetSelections": [
			{
				"name": "Monster-slaying weapons, pick three.",
				"gearList": [
					"Mallet & wooden stakes (3-harm intimate slow wooden) ",
					"Silver sword (2-harm hand messy silver)",
					"Cold iron sword (2-harm hand messy iron)",
					"Blessed knife (2-harm hand holy)",
					"Magical dagger (2-harm hand magic)",
					"Juju bag (1-harm far magic)",
					"Flamethrower (3-harm close fire heavy volatile)",
					"Magnum (3-harm close reload loud)",
					"Shotgun (3-harm close messy loud)"
				]
			}
		]
	},

	"ratings": [
		{
			"name": "Charm -1, Cool +1, Sharp +2, Tough +1, Weird +0",
			"charm": -1,
			"cool": 1,
			"sharp": 2,
			"tough": 1,
			"weird": 0
		},
		{
			"name": "Charm +0, Cool +1, Sharp +2, Tough -1, Weird +1",
			"charm": 0,
			"cool": 1,
			"sharp": 2,
			"tough": -1,
			"weird": 1
		},
		{
			"name": "Charm +1, Cool -1, Sharp +2, Tough +1, Weird +0",
			"charm": 1,
			"cool": -1,
			"sharp": 2,
			"tough": 1,
			"weird": 0
		},
		{
			"name": "Charm -1, Cool +1, Sharp +2, Tough +0, Weird +1",
			"charm": -1,
			"cool": 1,
			"sharp": 2,
			"tough": 0,
			"weird": 1
		},
		{
			"name": "Charm -1, Cool +0, Sharp +2, Tough -1, Weird +2",
			"charm": -1,
			"cool": 0,
			"sharp": 2,
			"tough": -1,
			"weird": 2
		}
	],
	
	"improvements": [
		"Get +1 Sharp (max +3)",
		"Get +1 Charm (max +2)",
		"Get +1 Cool (max +2)",
		"Get +1 Tough (max +2)",
		"Get +1 Weird (max +2)",
		"Take a move from another playbook",
		"Take a move from another playbook",
		"Take another Expert move",
		"Take another Expert move",
		"Add an option to your haven",
		"Add an option to your haven"
	],

	"advancedImprovements": [
		"Get +1 to any rating (max +3).",
		"Get back one used Luck point.",
		"Change this Hunter to a new archetype.",
		"Create a second Hunter to play as well as this one.",
		"Mark two of the basic moves as advanced.",
		"Mark another two of the basic moves as advanced.",
		"Retire this Hunter to safety."
	]
}
