{
	"name": "The Wronged",
	"desc": "They took my loved ones. Back then I wasn't strong enough to fight, but I studied, trained, and now I'm ready to cleanse the world of their taint. I'll kill them all. That's all I have left.",
	"luckSpecial": "When you spend a point of Luck, you find a dangerous lead on your prey.",

	"specialMechanics": [
		{
			"name": "Who You Lost",
			"desc": "Who did you lose? Pick one or more of:",
			"specialMechanicList": [
				"Your parent(s): [Input]",
				"You sibling(s): [Input]",
				"Your spouse/partner: [Input]",
				"Your child(ren): [Input]",
				"Your best friend(s): [Input]"
			]
		},
		{
			"name": "What Did It?",
			"desc": "With the Keeper's agreement, pick the monster breed \nMy prey: [Input] \nWhy couldn't you save them? You were (pick one or more):",
			"specialMechanicList": [
				"at fault",
				"selfish",
				"injured",
				"weak",
				"slow",
				"scared",
				"in denial",
				"complicit"
			]
		}
	],

	"moves": {
		"desc": "Pick two moves aside from preselected one.",
		"moveCount": 2,
		"moveList": [
			{
				"name": "I Know My Prey",
				"isPreselected": true,
				"desc": "You get +1 ongoing when knowingly investigating, pursuing or fighting the breed of monster that caused your loss."
			},
			{
				"name": "Berserk",
				"isPreselected": false,
				"desc": "No matter how much harm you take, you can always keep going until the current fight is over. During a fight, the Keeper may not use harm moves on you and you cannot die. When the fight ends, all harm takes effect as normal."
			},
			{
				"name": "NEVER AGAIN",
				"isPreselected": false,
				"desc": "In combat, you may choose to protect someone without rolling, as if you had rolled a 10+, but you may not choose to \"suffer little harm.\""
			},
			{
				"name": "What Does Not Kill Me...",
				"isPreselected": false,
				"desc": "If you have suffered harm in a fight, you gain +1 ongoing until the fight is over."
			},
			{
				"name": "Fervor",
				"isPreselected": false,
				"desc": "When you manipulate someone, roll +Tough instead of +Charm."
			},
			{
				"name": "Safety First",
				"isPreselected": false,
				"desc": "You have jury-rigged extra protection into your gear, giving you +1 armour (maximum 2-armour)."
			},
			{
				"name": "DIY Surgery",
				"isPreselected": false,
				"desc": "When you do quick and dirty first aid on someone (including yourself), roll +Cool. On a 10+ it's all good, it counts as normal first aid, plus stabilize the injury and heal 1 harm. On a 7-9 it counts as normal first aid, plus one of these, your choice: \n- Stabilise the injury but the patient takes -1 forward. \n- Heal 1-harm and stabilise for now, but it will return as 2-harm and become unstable again later. \n- Heal 1-harm and stabilise but the patient takes -1 ongoing until it's fixed properly."
			},
			{
				"name": "Tools Matter",
				"isPreselected": false,
				"desc": "With your signature weapon, you get +1 to kick some ass"
			}
		]
	},

	"gears": {
		"desc": "Pick one signature weapon and two practical weapons. You have protective wear, suited to your look, worth 1-armour. If you want, you may take a classic car, classic motorcycle, plain pickup, or plain van.",
		"gearSetSelections": [
			{
				"name": "Signature Weapons, pick one.",
				"gearList": [
					"Sawn-off shotgun (3-harm hand/close messy loud reload)",
					"Hand cannon (3-harm close loud)",
					"Fighting knife (2-harm hand quiet)",
					"Huge sword or huge axe (3-harm hand messy heavy)",
					"Specialist weapons for destroying your foes (e.g. wooden stakes and mallet for vampires, silver dagger for werewolves, etc.). 4-harm against the specific creatures it targets, 1-harm otherwise, and other tags by agreement with the Keeper.",
					"Enchanted dagger (2-harm hand magic)",
					"Chainsaw (3-harm hand messy unreliable loud heavy)"
				]
			},
			{
				"name": "Practical Weapons, pick two.",
				"gearList": [
					".38 revolver (2-harm close reload loud)",
					"9mm (2-harm close loud)",
					"Hunting rifle (2-harm far loud)",
					"Shotgun (3-harm close messy loud)",
					"Big knife (1-harm hand)",
					"Brass knuckles (1-harm hand stealthy)",
					"Assault rifle (3-harm close area loud reload)"
				]
			}
		]
	},

	"ratings": [
		{
			"name": "Charm +0, Cool +1, Sharp -1, Tough +2, Weird +1",
			"charm": 0,
			"cool": 1,
			"sharp": -1,
			"tough": 2,
			"weird": 1
		},
		{
			"name": "Charm +0, Cool +0, Sharp +1, Tough +2, Weird +0",
			"charm": 0,
			"cool": 0,
			"sharp": 1,
			"tough": 2,
			"weird": 0
		},
		{
			"name": "Charm +1, Cool +0, Sharp +1, Tough +2, Weird -1",
			"charm": 1,
			"cool": 0,
			"sharp": 1,
			"tough": 2,
			"weird": -1
		},
		{
			"name": "Charm -1, Cool -1, Sharp +0, Tough +2, Weird +2",
			"charm": -1,
			"cool": -1,
			"sharp": 0,
			"tough": 2,
			"weird": 2
		},
		{
			"name": "Charm +1, Cool -1, Sharp +0, Tough +2, Weird +1",
			"charm": 1,
			"cool": -1,
			"sharp": 0,
			"tough": 2,
			"weird": 1
		}
	],
	
	"improvements": [
		"Get +1 Cool (max +2)",
		"Get +1 Sharp (max +2)",
		"Get +1 Tough (max +3)",
		"Get +1 Weird (max +2)",
		"Take a move from another playbook",
		"Take a move from another playbook",
		"Take another Wronged move",
		"Take another Wronged move",
		"Gain a haven, like the Exert has, with two options",
		"Add one more option to your haven"
	],

	"advancedImprovements": [
		"Get +1 to any rating (max +3).",
		"Get back one used Luck point.",
		"Change this Hunter to a new archetype.",
		"Create a second Hunter to play as well as this one.",
		"Mark two of the basic moves as advanced.",
		"Mark two of the basic moves as advanced.",
		"Retire this Hunter to safety.",
		"You track down the specific monster(s) responsible for your loss. The Keeper must make the next mystery about them.",
		"Change the target of your vengeful rage. Pick a new monster breed: I know my prey now applies to them instead."
	]
}