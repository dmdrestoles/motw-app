{
	"name": "The Monstrous",
	"desc": "I feel the hunger, the lust to destroy. But I fight it: I never give in. I'm not human any more, not really, but I have to protect those who still are. That way I can tell myself I'm different to the other monsters. Sometimes I can even believe it.",
	"luckSpecial": "When you spend a point of Luck, your monster side gains power: your Curse may become stronger, or another Breed disadvantage may manifest.",

	"specialMechanics": [
		{
			"name": "Monster Breed",
			"desc": "You're half-human, half-monster: decide if you were always this way or if you you were originally human and transformed somehow. Now decide if you were always fighting to be good, or if you were evil and changed sides. Define your monstrous breed by picking a curse, moves, and natural attacks. Create the monster you want to be: whatever you choose defines your breed in the game.",
			"specialMechanicList": [
				{
					"name": "Curses, pick one",
					"selection": [
						"Feed - You must subsist on living humans. It might take the form of blood, brains, or spiritual essence but it must be from people. You need to act under pressure to resist feeding whenever a perfect opportunity presents itself.",
						"Vulnerability - Pick a substance. You suffer +1 harm when  you suffer harm from it. If you are bound or surrounded by it, you must act under pressure to use your powers.",
						"Pure Drive - One emotion rules you. Pick from: hunger, hate, anger, fear, jealousy, greed, joy, pride, envy, lust, or cruelty. Whenever you have a chance to indulge that emotion, you must do so immediately, or act under pressure to resist.",
						"Dark Master - You have an evil lord who doesn't know you changed sides. They still give you orders, and they do not tolerate refusal. Or failure."
					]
				},
				{
					"name": "Natural Attacks: (Pick a Base+ Extra or two Bases)",
					"selection": [
						"Base: teeth (3-harm intimate)",
						"Base: claws (2-harm hand)",
						"Base: magical force (1-harm magical close)",
						"Base: life-drain (1-harm intimate life-drain)",
						"Extra: Add +1 harm to a base",
						"Extra: Add ignore-armour to a base",
						"Extra: Add an extra range to a base (add intimate, hand, or close)"
					]
				}
			]
		}
	],

	"moves": {
		"desc": "Pick two moves.",
		"moveCount": 2,
		"moveList": [
			{
				"name": "Immortal",
				"isPreselected": false,
				"desc": "You do not age or sicken, and whenever you suffer harm you suffer 1-harm less."
			},
			{
				"name": "Unnatural Appeal (Weird)",
				"isPreselected": false,
				"desc": "Roll +Weird instead of +Charm when you manipulate someone."
			},
			{
				"name": "Unholy Strength (Weird)",
				"isPreselected": false,
				"desc": "Roll +Weird instead of +Tough when you kick some ass."
			},
			{
				"name": "Incorporeal",
				"isPreselected": false,
				"desc": "You may move freely through solid objects (but not people)."
			},
			{
				"name": "Preternatural Speed",
				"isPreselected": false,
				"desc": "You go much faster than normal people. When you chase, flee, or run take +1 ongoing."
			},
			{
				"name": "Claws of the Beast",
				"isPreselected": false,
				"desc": "All your natural attacks get +1 harm."
			},
			{
				"name": "Mental Dominion",
				"isPreselected": false,
				"desc": "When you gaze into a normal human's eyes and exert your will over them, roll +Charm. On a 10+, hold 3. On a 7-9, hold 1. You may spend your hold to give them an order. Regular people will follow your order, whatever it is. Hunters can choose whether they do it or not. If they do, they mark experience."
			},
			{
				"name": "Unquenchable Vitality",
				"isPreselected": false,
				"desc": "When you have taken harm, you can heal yourself. Roll +Cool. On a 10+, heal 2-harm and stabilise your injuries. On a 7-9, heal 1-harm and stabilise your injuries. On a miss, your injuries worsen."
			},
			{
				"name": "Dark Negotiator",
				"isPreselected": false,
				"desc": "You can use the Manipulate Someone move on monsters as well as people, if they can reason and talk."
			},
			{
				"name": "Flight",
				"isPreselected": false,
				"desc": "You can fly."
			},
			{
				"name": "Shapeshifter",
				"isPreselected": false,
				"desc": "You may change your form (usually into an animal). Decide if you have just one alternate form or several, and detail them. You gain +1 to investigate a mystery when using an alternate form's superior senses (e.g. smell for a wolf, sight for an eagle)."
			},
			{
				"name": "Something Borrowed",
				"isPreselected": false,
				"desc": "Take a move from a hunter playbook that is not currently in play."
			}
		]
	},

	"gears": {
		"desc": "If you want, you can take one handy weapon.",
		"gearSetSelections": [
			{
				"name": "Handy weapon, pick one.",
				"gearList": [
					".38 revolver (2-harm close reload loud)",
					"9mm (2-harm close loud)",
					"Magnum (3-harm close reload loud)",
					"Shotgun (3-harm close messy)",
					"Big knife (1-harm hand)",
					"Brass knuckles (1-harm hand quiet small)",
					"Sword (2-harm hand messy)",
					"Huge sword (3-harm hand heavy)"
				]
			}
		]
	},

	"ratings": [
		{
			"name": "Charm -1, Cool -1, Sharp +0, Tough +2, Weird +3",
			"charm": -1,
			"cool": -1,
			"sharp": 0,
			"tough": 2,
			"weird": 3
		},
		{
			"name": "Charm -1, Cool +1, Sharp +1, Tough +0, Weird +3",
			"charm": -1,
			"cool": 1,
			"sharp": 1,
			"tough": 0,
			"weird": 3
		},
		{
			"name": "Charm +2, Cool +0, Sharp -1, Tough -1, Weird +3",
			"charm": 2,
			"cool": 0,
			"sharp": -1,
			"tough": -1,
			"weird": 3
		},
		{
			"name": "Charm -2, Cool +2, Sharp +0, Tough +0, Weird +3",
			"charm": -2,
			"cool": 2,
			"sharp": 0,
			"tough": 0,
			"weird": 3
		},
		{
			"name": "Charm +0, Cool -1, Sharp +2, Tough -1, Weird +3",
			"charm": 0,
			"cool": -1,
			"sharp": 2,
			"tough": -1,
			"weird": 3
		}
	],
	
	"improvements": [
		"Get +1 Charm (max +2)",
		"Get +1 Cool (max +2)",
		"Get +1 Sharp (max +2)",
		"Get +1 Tough (max +2)",
		"Take a move from another playbook",
		"Take a move from another playbook",
		"Take another Monstrous move",
		"Take another Monstrous move",
		"Gain a haven, like the Expert has, with two options",
		"Take a natural attacks pick"
	],

	"advancedImprovements": [
		"Get +1 to any rating (max +3)",
		"Get back one used Luck point",
		"Change this Hunter to a new archetype",
		"Create a second Hunter to play as well as this one",
		"Mark two of the basic moves as advanced",
		"Mark two of the basic moves as advanced",
		"Retire this Hunter to safety",
		"Free yourself from the curse of your kind. Your curse no longer applies, but you lose 1 Weird",
		"You turn evil (again). Retire this character, they become one of the Keeper's threats"
	]
}
