{
	"name": "The Hex",
	"desc": "I didn't have magic fall into my lap. I'm not blessed, I'm not one of the scary children-I'm just a girl who found a way to give herself the strength to fight this war. I don't have the option of not taking this risk.",
	"luckSpecial": "When you spend Luck, until the end of the mystery, backlash on your spells will be extra nasty.",

	"specialMechanics": [
		{
			"name": "Temptation",
			"desc": "You have a dangerous drive that you pursue, sometimes to the exclusion of your own safety. Decide if your Temp-tation drove you to learn magic, or if learning magic drove you to it. Whenever you give in to your Temptation and act accordingly, you mark experience. You need to act under pressure to resist giving in to your temptation, if a perfect opportunity presents itself; if you fail this roll, you don't mark experience like you would have if you'd willingly acted out your desires.\n\nChoose one Temptation:",
			"specialMechanicList": [
				"Vengeance - Use magic to inflict disproportionate retribution on someone who wronged you.",
				"Power - Use magic to exert your dominance over another.",
				"Addiction - Use magic to do what you could do without it.",
				"Callousness - Use magic without regard for the safety of others.",
				"Carnage - Use magic to inflict gruesome violence.",
				"Secrets - Use magic to discover forbidden, dangerous knowledge.",
				"Glory - Use magic to steal someone's thunder."
			]
		},
		
		{
			"name": "Rotes",
			"desc": "Whenever you use magic, you can decide afterwards that a particular spell is a rote that you know. Write down in detail what the spell does, and what it requires. You know how to cast it off the top of your head, and you choose two requirements.\n\nGive your new rote a name, and decide specifically what the requirements are (which words, gestures, objects, symbols, and procedures are required). Unlike regular use magic, a rote's cost and the consequences for failing it are known to you in advance. After you cast it for the first time, treat each rote as a custom move-a specialised version of use magic, which is built with the Keeper. Write down what it does on a 10+, a 7-9, and a miss. Also, a rote is a bit more powerful than a basic use magic spell: its glitches are less onerous and its effect may be a little bigger. \n\nCasting a rote requires you to have the needed items at hand and the ability to physically use them. You roll +Weird to cast it, as you would when using magic normally.\n\nYou start out knowing up to one rote, which you can choose when creating your character or during play. You can learn more by taking improvements-when you do, you can choose the new rote right away or in play.\n\n[Input]Rote Name:\n[Input]On a 10+:\nOn a 7-9+\nOn a miss:\nRequirements (pick two):",
			"specialMechanicList": [
				"Magic words, ritual gestures",
				"Object of power which must be wielded",
				"Expendable component destroyed or scattered",
				"Runes or symbols written or engraved on a surface",
				"Spilling of blood (1-harm to you or willing person)"
			]
		},
		{
			"name": "Advanced Hex Moves",
			"desc": "When you take the \"Choose one advanced Hex move\" improvement, choose one of these moves. You can never have both:",
			"specialMechanicList": [
				"Apotheosis - You become a terrifying fount of mystical power. Once per mystery, after you suffer loss or harm, you may enter a state where you have both immense power and zero interest in the well-being of other people. While in this state, you can fly, use +Weird to kick some ass instead of +Tough (using innate magic as a 3-harm weapon with whatever properties you choose), ignore the component costs of your rotes, ignore one requirement of every spell you cast with use magic, and you have +1 ongoing to do everything. On the other hand, you outright cannot use the protect someone move, you have all the Temptations, and you have to indulge them whenever a good opportunity presents itself. When you try to resist a temptation, roll +Cool. On a 10+, your apotheosis ends. On a 7-9, it ends with you doing something dangerous or cruel. On a miss, it ends only after you harm someone (or something) you love.",
				"Synthesis - You manage to conciliate your dark power with your moral impulses. You lose your Temptation. Whenever you use magic to help outor protect someone, you mark experience. Mark a second experience if you do it at the expense of your own safety."
			]
		}
	],

	"moves": {
		"desc": "Pick two aside from the preselected one.",
		"moveCount": 2,
		"moveList": [
			{
				"name": "Bad Luck Charm",
				"isPreselected": true,
				"desc": "Whenever you use magic and miss, the backlash never affects you directly if there's someone else around to hit. It'll go for allies, other hunters, and innocent bystanders. Sometimes, every so often, it might even hit an enemy."
			},
			{
				"name": "Burn Everything",
				"isPreselected": false,
				"desc": "When you use magic to inflict harm, you can choose to inflict 3-harm area magic obvious or 3-harm ignore-armour magic obvious."
			},
			{
				"name": "Cast the Bones (Sharp)",
				"isPreselected": false,
				"desc": "Once per mystery, you may perform some kind of divination (tarot, casting the runes, reading entrails, or something like that) to glean information about the future. When you seek guidance by divination, roll +Sharp. On a 10+, hold 2. On a 7-9, hold 1. On a miss, you get some information, but it's not what you want to hear. Spend those holds to ask any question from the investigate a mystery move, or one of the following questions.The Keeper will answer truthfully, with either a direct answer or how to find out more.:\n-> What can I gain from this person/place/thing/creature?\n-> Who has touched this person/place/thing/creature before me?"
			},
			{
				"name": "Force of Will (Weird)",
				"isPreselected": false,
				"desc": "When you apply your will to dispelling a magical effect, blocking a spell, or suspending a phenomenon, roll +Weird. On a hit, momentary magics are cancelled completely, and long-lasting spells and effects are suspended temporarily. On a 10+, you can also spend Luck to instantly snuff out a powerful spell or strange effect. On a 7-9, you take 1-harm as the strain of dismissing magic unravels you."
			},
			{
				"name": "Luck of the Damned",
				"isPreselected": false,
				"desc": "After you use magic or cast a rote, take +1 forward on the next roll you make."
			},
			{
				"name": "Sympathetic Token",
				"isPreselected": false,
				"desc": "As long as you carry a personal object belonging to someone, such as a lock of hair, a full set of toenails, or a treasured family heirloom, you get +1 ongoing to use magic against them. You can also use magic against them at a distance. If you try to use magic against them and miss, the token is lost, destroyed, or loses its power."
			},
			{
				"name": "This Might Sting",
				"isPreselected": false,
				"desc": "You can use magic to heal 3-harm, but the process is exceptionally painful. On a 7-9 it also leaves a gnarly scar."
			},
			{
				"name": "Wise Soul",
				"isPreselected": false,
				"desc": "Whenever you use magic, right before you roll, you can ask the Keeper what exactly would happen on a miss. If you dislike the risk, you can stop at the last second, and let the spell fizzle harmlessly. All of the effort is wasted."
			}
		]
	},

	"gears": {
		"desc": "You have whatever magical items or amulets you use to perform magic, including whatever you need to cast your rotes. You also have two wizardly weapons.",
		"gearSetSelections": [
			{
				"name": "Wizardly weapons, pick two",
				"gearList": [
					".38 revolver (2-harm close reload loud)",
					"Shotgun (3-harm close messy loud)",
					"Athame (2-harm hand magic silver)",
					"Shillelagh (1-harm hand balanced)",
					"Crossbow (2-harm close slow)",
					"Staff (1-harm hand balanced large)"
				]
			}
		]
	},

	"ratings": [
		{
			"name": "Charm +2, Cool +0, Sharp +0, Tough -1, Weird +2",
			"charm": 2,
			"cool": 0,
			"sharp": 0,
			"tough": -1,
			"weird": 2
		},
		{
			"name": "Charm +1, Cool –1, Sharp +1, Tough +0, Weird +2",
			"charm": 1,
			"cool": -1,
			"sharp": 1,
			"tough": 0,
			"weird": 2
		},
		{
			"name": "Charm –1, Cool +1, Sharp +0, Tough +1, Weird +2",
			"charm": -1,
			"cool": 1,
			"sharp": 0,
			"tough": 1,
			"weird": 2
		},
		{
			"name": "Charm –1, Cool +0, Sharp +1, Tough +1, Weird +2",
			"charm": -1,
			"cool": 0,
			"sharp": 1,
			"tough": 1,
			"weird": 2
		},
		{
			"name": "Charm +0, Cool +0, Sharp +2, Tough –1, Weird +2",
			"charm": 0,
			"cool": 0,
			"sharp": 2,
			"tough": -1,
			"weird": 2
		}
	],
	
	"improvements": [
		"Get +1 Charm (max +2)",
		"Get +1 Cool (max +2)",
		"Get +1 Sharp (max +2)",
		"Get +1 Weird (max +3)",
		"Take another Rote",
		"Take another Rote",
		"Take another Rote",
		"Take a move from another playbook",
		"Take another Hex move, or an additional Rote",
		"Take another Hex move, or an additional Rote",
		"Take a Haven like the Expert has, with two options"
	],

	"advancedImprovements": [
		"Get +1 to any rating (max +3).",
		"Get back one used Luck point.",
		"Change this Hunter to a new archetype.",
		"Create a second Hunter to play as well as this one.",
		"Mark two of the basic moves as advanced.",
		"Mark two of the basic moves as advanced.",
		"Retire this Hunter to safety.",
		"Gain another two rotes.",
		"Choose one advanced Hex move."
	]
}
