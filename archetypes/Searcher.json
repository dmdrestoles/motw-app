{
	"name": "The Searcher",
	"desc": "There's still so much to be discovered and explained, even now. Perhaps only one event in a thousand is true weirdness-but I'll investigate them all to find it.",
	"luckSpecial": "When you spend a point of Luck, your first encounter comes up in play. It could be a flashback, new occurrence, or related event.",

	"specialMechanics": [],

	"moves": {
		"desc": "Pick two aside from the preselected one.",
		"moveCount": 2,
		"moveList": [
			{
				"name": "[Selection] First Encounter",
				"isPreselected": true,
				"desc": "One strange event started you down this path, sparking your need to discover the truth behind the unexplained. Decide what that event was: pick a category below and take the associated move. Then tell everyone what happened to you (or someone close to you).\nCryptid Sighting:\nYou take note of any reports of strange creatures. Whenever you first see a new type of creature, you may immediately ask one of the investigate a mystery questions.\n\nZone of Strangeness:\nThings are not fixed. You never need act under pressure when supernatural forces alter the environment around you, and you get 2-armour against harm from sudden changes to the laws of physics.\n\nPsychic Event:\nYour mind is awakened. You may act under pressure to use the 'sensitive' weird move, or-if 'sensitive' is your weird move-'empath'. See page 21 of Tome of Mysteries.\n\nHigher power:\nSomething looks out for you. You start with an extra Luck.\n\nStrange Dangers:\nYou are always watching for hazards. When you have no armour, you still count as having 1-armour\n\nAbduction:\nThey taught you hidden knowledge. Gain +1 to any move when you research strange or ancient secrets to do it.\n\nCosmic Insight:\nYou have encompassed the soul of the universe. You never need to act under pressure due to feelings of fear, despair, or isolation."
			},
			{
				"name": "Prepared to Defend",
				"isPreselected": false,
				"desc": "Even truth seekers need to fight some times. Whenever you suffer harm when you kick some ass or protect someone, you suffer 1-harm less."
			},
			{
				"name": "Fellow Believer",
				"isPreselected": false,
				"desc": "People understand you've also known strangeness. Bystanders will talk to you about weird things they would not trust another hunter (or a mundane official) to believe."
			},
			{
				"name": "Guardian",
				"isPreselected": false,
				"desc": "You have a mystical ally (perhaps a spirit, alien, or cryptid) who helps and defends you. Define them, and their powers, with the Keeper's agree-ment. Their look is one of: invisible, an intangible spirit thing, a weird creature, disguised as an animal, or disguised as a person."
			},
			{
				"name": "Just Another Day",
				"isPreselected": false,
				"desc": "When you have to act under pressure due to a monster, phenomenon, or mystical effect, you may roll +Weird instead of +Cool."
			},
			{
				"name": "Network",
				"isPreselected": false,
				"desc": "You may gain an ally group of others who had experiences similar to your first encounter-perhaps they're a support group or hobbyist club. Detail up to five members with useful skills related to what happened to them (none are up for fighting monsters)."
			},
			{
				"name": "Ockham's Broadsword",
				"isPreselected": false,
				"desc": "When you first encounter something strange, you may ask the Keeper what sort of thing it is. They will tell you if it (or the cause) is: natural, an unnatural creature, a weird phenomenon, or a person. You gain +1 forward dealing with it."
			},
			{
				"name": "The Things I've Seen",
				"isPreselected": false,
				"desc": "When you encounter a creature or phenomenon, you may declare that you have seen it before. The Keeper may ask you some questions about that encounter, and will then tell you one useful fact you learned and one danger you need to watch out for (maybe right now)."
			}
		]
	},

	"gears": {
		"desc": "You get a laptop, a car or motorcycle, a camera, binoculars, two sets of investigation tools, and one self-defence weapon.",
		"gearSetSelections": [
			{
				"name": "Investigation tools, pick two.",
				"gearList": [
					"A bag of cameras & microphones.",
					"Forensic tools.",
					"Ghost hunting tools.",
					"Scientific measuring tools.",
					"Cryptid hunting gear.",
					"Historical documents and witness reports of strange events.",
					"Maps, blueprints, and building reports for significant places."
				]
			},
			{
				"name": "Self-defence weapons, pick one.",
				"gearList": [
					"Walking stick (1-harm hand innocuous)",
					"Small handgun (2-harm close reload loud)",
					"Small knife (1-harm hand messy)",
					"Martial arts training (1-harm hand innocuous)",
					"Incapacitating spray (0-harm hand irritating)",
					"Heavy flashlight (1-harm hand innocuous)"
				]
			}
		]
	},

	"ratings": [
		{
			"name": "Charm +2, Cool +0, Sharp +1, Tough +0, Weird +0",
			"charm": 2,
			"cool": 0,
			"sharp": 1,
			"tough": 0,
			"weird": 0
		},
		{
			"name": "Charm +2, Cool +0, Sharp +1, Tough +1, Weird -1",
			"charm": 2,
			"cool": 0,
			"sharp": 1,
			"tough": 1,
			"weird": -1
		},
		{
			"name": "Charm +1, Cool +0, Sharp +2, Tough +1, Weird -1",
			"charm": 1,
			"cool": 0,
			"sharp": 2,
			"tough": 1,
			"weird": -1
		},
		{
			"name": "Charm +1, Cool -1, Sharp +2, Tough +0, Weird +1",
			"charm": 1,
			"cool": -1,
			"sharp": 2,
			"tough": 0,
			"weird": 1
		},
		{
			"name": "Charm +2, Cool +1, Sharp +1, Tough +0, Weird -1",
			"charm": 2,
			"cool": 1,
			"sharp": 1,
			"tough": 0,
			"weird": -1
		}
	],
	
	"improvements": [
		"Get +1 Charm (max +2)",
		"Get +1 Cool (max +2)",
		"Get +1 Sharp (max +3)",
		"Get +1 Weird (max +3)",
		"Take a move from another playbook",
		"Take a move from another playbook",
		"Take another Searcher move",
		"Take another Searcher move",
		"Take a second First Encounter move based on a recent mystery",
		"Gain an ally"
	],

	"advancedImprovements": [
		"Get +1 to any rating (max +3).",
		"Get back one used Luck point.",
		"Get back one used Luck point.",
		"Change this Hunter to a new archetype.",
		"Create a second Hunter to play as well as this one.",
		"Mark two of the basic moves as advanced.",
		"Mark two of the basic moves as advanced.",
		"Retire this Hunter to safety.",
		"Resolve your first encounter. THe Keeper makes the next mystery about this event, and should try to answer all remaining questions about it during the mystery (although there are sure to be new threads to investigate after...)."
	]
}
