import java.util.*;
import java.io.*;
import org.json.*;
import java.lang.reflect.Field;
import java.util.LinkedHashMap;

public class FileLoader{

	// FIELDS
	private File archetypesFolder = new File("." + File.separator + "archetypes" + File.separator);
	private File charactersFolder = new File("." + File.separator + "characters" + File.separator);
	private File compendiumFolder = new File("." + File.separator + "compendium" + File.separator);
	private File[] archetypeData;
	private File[] characterData;
	private File[] compendiumData;

	private ArrayList<Archetype> archetypes;
	private ArrayList<Character> characters;
	private ArrayList<BasicMove> basicMoves;

	// CONSTRUCTORS
	public FileLoader(){
		archetypes = new ArrayList<>();
		characters = new ArrayList<>();
		basicMoves = new ArrayList<>();
	}


	// METHODS
	public ArrayList<Archetype> GetArchetypes(){
		return archetypes;
	}

	public ArrayList<Character> GetCharacters(){
		System.out.println( characters.size() );
		return characters;
	}

	public ArrayList<BasicMove> GetBasicMoves(){
		return basicMoves;
	}
	
	private static void makeJSONObjLinear(JSONObject jsonObject) {
		try {
			Field changeMap = jsonObject.getClass().getDeclaredField("map");
			changeMap.setAccessible(true);
			changeMap.set(jsonObject, new LinkedHashMap<>());
			changeMap.setAccessible(false);
		} 
		catch (IllegalAccessException | NoSuchFieldException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Method for opening a file
	 * @param File file
	 * @return new Reader
	 */
	public static Reader OpenFile(File file) throws IOException{
		return new FileReader(file);
	}

	/**
	 * Reads every file located in the folder specified, and instantiates the Archetypes inside those files
	 * @param File file
	 */
	public String ReadFile(File file) throws IOException{
		Scanner br = null;
		String fileContentInString = new String();
		//System.out.println( file.getName() );

		try{
			br = new Scanner(OpenFile(file));

			while ( br.hasNext() ){
				fileContentInString += br.nextLine();
			}

		}

		finally{
			br.close();
		}

		return fileContentInString;
	}

	public void WriteCharacterToFile( Character c ) throws IOException{
		JSONObject characterObject = new JSONObject();
		makeJSONObjLinear( characterObject );
		FileWriter file = new FileWriter( charactersFolder.getPath() + File.separator + c.GetCharacterName() + ".json" );

		characterObject.put( "name", c.GetCharacterName() );
		characterObject.put( "desc", c.GetCharacterDesc() );

		characterObject.put( "harm", c.GetHarm() );
		characterObject.put( "luck", c.GetLuck() );
		characterObject.put( "forward", c.GetForward() );
		characterObject.put( "experience", c.GetExperience() );
		characterObject.put( "armor", c.GetArmor() );
		characterObject.put( "inventory", c.GetInventory() );

		JSONObject characterArchetype = new JSONObject();
		makeJSONObjLinear( characterArchetype );
		CharacterArchetype ca = c.GetCharacterArchetype();

		characterArchetype.put( "name", ca.GetName() );
		characterArchetype.put( "desc", ca.GetDesc() );
		characterArchetype.put( "luckSpecial", ca.GetLuckSpecial() );

		JSONArray specialMechanics = new JSONArray();
		ArrayList<CharacterSpecialMechanic> csmList = ca.GetSpecialMechanics();

		for ( CharacterSpecialMechanic csm : csmList ){
			JSONObject obj = new JSONObject();
			makeJSONObjLinear(obj);
			obj.put( "name", csm.GetMechanicName() );
			obj.put( "desc", csm.GetMechanicDesc() );
			obj.put( "specialMechanicList", csm.GetSpecialMechanicEntry() );

			specialMechanics.put( obj );
		}

		characterArchetype.put( "specialMechanics", specialMechanics );
		// characterArchetype.put( "moves", ca.GetMoves() );

		JSONArray moves = new JSONArray();

		for ( Move m : ca.GetMoves() ){
			JSONObject obj = new JSONObject();
			makeJSONObjLinear( obj );

			obj.put( "name", m.GetMoveName() );
			obj.put( "isPreselected", m.IsPreselected() );
			obj.put( "desc", m.GetMoveDesc() );

			moves.put( obj );
		}
		characterArchetype.put( "moves", moves );
		characterArchetype.put( "gears", ca.GetGears() );
		
		JSONObject characterRating = new JSONObject();
		makeJSONObjLinear( characterRating );
		Rating r = ca.GetRatings();
		characterRating.put( "name", r.GetRatingName() );
		characterRating.put( "charm", r.GetCharm() );
		characterRating.put( "cool", r.GetCool() );
		characterRating.put( "sharp", r.GetSharp() );
		characterRating.put( "tough", r.GetTough() );
		characterRating.put( "weird", r.GetWeird() );

		characterArchetype.put( "ratings", characterRating );
		characterArchetype.put( "improvements", ca.GetImprovements() );
		characterArchetype.put( "advancedImprovements", ca.GetAdvancedImprovements() );

		characterObject.put( "characterArchetype", characterArchetype );

		try {
			file.write( characterObject.toString(4) );
		}
		catch (IOException ioe){
			ioe.printStackTrace();
		}
		finally{
			try {
				file.flush();
				file.close();
			}
			catch( IOException ioe ){
				ioe.printStackTrace();
			}
		}

	}

	/**
	 * Adds an Archetype based on the file being read.
	 * Uses helper functions to process each unique JSON key.
	 * @param fileContent
	 */
	public void AddArchetype( String fileContent ){
		JSONObject archetypeObject = new JSONObject( fileContent );
		String name = archetypeObject.getString("name");
		String desc = archetypeObject.getString("desc");
		String luckSpecial = archetypeObject.getString("luckSpecial");
		
		Archetype archetype = new Archetype( name, desc );
		archetype.SetArchetypeLuckSpecial( luckSpecial );
		archetypes.add( archetype );

		ArrayList<SpecialMechanic> archetypeSpecialMechanics = AddSpecialMechanic( archetypeObject.getJSONArray( "specialMechanics" ) );
		archetype.SetSpecialMechanics( archetypeSpecialMechanics );
		
		MoveList archetypeMoveList = AddMove( archetypeObject.getJSONObject( "moves" ) );
		archetype.SetMoveList( archetypeMoveList );

		// System.out.println( "Starting addition of GearSet to " + archetype.GetArchetypeName() );
		GearSet archetypeGearSet = AddGearSet( archetypeObject.getJSONObject( "gears" ) );
		archetype.SetGearSet( archetypeGearSet );

		ArrayList<Rating> archetypeRatings = AddRating( archetypeObject.getJSONArray( "ratings" ) );
		archetype.SetRatings( archetypeRatings );

		ArrayList<String> archetypeImprovements = AddImprovement( archetypeObject.getJSONArray( "improvements" ) );
		archetype.SetImprovements( archetypeImprovements );

		ArrayList<String> archetypeAdvancedImprovements = AddAdvancedImprovement( archetypeObject.getJSONArray( "advancedImprovements" ) );
		archetype.SetAdvancedImprovements( archetypeAdvancedImprovements );
	}

	public Character AddCharacter( String fileContent ){
		JSONObject characterObject = new JSONObject( fileContent );
		String characterName = characterObject.getString( "name" );
		String characterDesc = characterObject.getString( "desc" );

		Character character = new Character( characterName, characterDesc );

		characters.add( character );

		int harm = characterObject.getInt( "harm" );
		int luck = characterObject.getInt( "luck" );
		int forward = characterObject.getInt( "forward" );
		int experience = characterObject.getInt( "experience" );
		int armor = characterObject.getInt( "armor" );

		String inventory = characterObject.getString( "inventory" );
		
		CharacterArchetype characterArchetype = AddCharacterArchetype( characterObject.getJSONObject( "characterArchetype" ) );

		character.SetCharacterArchetype( characterArchetype );
		character.SetHarm( harm );
		character.SetLuck( luck );
		character.SetForward( forward );
		character.SetExperience( experience );
		character.SetArmor( armor );
		character.SetInventory( inventory );

		return character;
	}

	/**
	 * Processes the Compendium file and prints it.
	 * @param fileContent
	 */
	public void AddCompendium( String fileContent ){
		JSONArray compendium = new JSONArray( fileContent );

		for ( Object obj : compendium ){
			JSONObject entry = (JSONObject) obj;

			String title = entry.getString( "title" );
			String desc = entry.getString( "desc" );

			// System.out.println( title );
			// System.out.println( desc );
			// System.out.printf( "\n" );

			JSONArray list = entry.getJSONArray( "list" );

			for ( Object move : list ){
				JSONObject content = (JSONObject) move;

				String moveName = content.getString( "name" );
				String moveDesc = content.getString( "desc" );
				String on7 = content.getString( "on7" );
				String on10 = content.getString( "on10" );
				String advanced = content.getString( "advanced" );

				// System.out.println( moveName );
				// System.out.println( moveDesc );
				// System.out.println( "  " + on7 );
				// System.out.println( "  " + on10 );
				// System.out.println( "  Advanced: " + advanced );

				if ( title.equals( "Basic Moves" ) ){
					BasicMove bm = new BasicMove();
					bm.name = moveName;
					bm.desc = moveDesc;
					bm.on7 = on7;
					bm.on10 = on10;
					bm.advanced = advanced;

					basicMoves.add( bm );
				}
			}
		}
	} 

	public CharacterArchetype AddCharacterArchetype( JSONObject characterArchetypeObject ){
		String archetypeName = characterArchetypeObject.getString( "name" );
		String archetypeDesc = characterArchetypeObject.getString( "desc" );

		CharacterArchetype archetype = new CharacterArchetype( archetypeName, archetypeDesc );
		String luckSpecial = characterArchetypeObject.getString( "luckSpecial" );
		JSONArray moves = characterArchetypeObject.getJSONArray( "moves" );
		String gears = characterArchetypeObject.getString( "gears" );
		String improvements = characterArchetypeObject.getString( "improvements" );
		String advancedImprovements = characterArchetypeObject.getString( "advancedImprovements" );

		ArrayList<CharacterSpecialMechanic> specialMechanics = new ArrayList<>();
		JSONArray specialMechanicsJSON = characterArchetypeObject.getJSONArray( "specialMechanics" );

		for ( int i = 0; i < specialMechanicsJSON.length(); i++ ){
			JSONObject entry = specialMechanicsJSON.getJSONObject(i);
			String mechanicName = entry.getString( "name" );
			String mechanicDesc = entry.getString( "desc" );
			String mechanicList = entry.getString( "specialMechanicList" );

			CharacterSpecialMechanic csm = new CharacterSpecialMechanic( mechanicName, mechanicDesc, mechanicList );

			specialMechanics.add( csm );
		}

		ArrayList<Move> movesList = new ArrayList<>();

		for ( int i = 0; i < moves.length(); i++ ){
			JSONObject entry = moves.getJSONObject(i);
			String moveName = entry.getString( "name" );
			boolean isPreselected = entry.getBoolean( "isPreselected" );
			String moveDesc = entry.getString( "desc" );

			Move m = new Move( moveName, isPreselected, moveDesc );

			movesList.add( m );
		}

		JSONObject ratingJSON = characterArchetypeObject.getJSONObject( "ratings" );

		String ratingName = ratingJSON.getString( "name" );
		int charm = ratingJSON.getInt( "charm" );
		int cool = ratingJSON.getInt( "cool" );
		int sharp = ratingJSON.getInt( "sharp" );
		int tough = ratingJSON.getInt( "tough" );
		int weird = ratingJSON.getInt( "weird" );

		Rating rating = new Rating( ratingName, charm, cool, sharp, tough, weird );
		
		archetype.SetLuckSpecial( luckSpecial );
		archetype.SetMoves( movesList );
		archetype.SetGears( gears );
		archetype.SetImprovements( improvements );
		archetype.SetAdvancedImprovements( advancedImprovements );
		archetype.SetSpecialMechanics( specialMechanics );
		archetype.SetRatings( rating );

		return archetype;
	}
	/**
	 * Helper function for adding all the entries in the "gears" key of the Archetype JSON file.
	 * @param gearObject
	 * @return GearSet gearSet
	 */
	public GearSet AddGearSet( JSONObject gearObject ){
		String gearSetDesc = gearObject.getString( "desc" );
		GearSet archetypeGearSet = new GearSet( gearSetDesc );
		ArrayList<Gear> gearSetSelection = new ArrayList<>();

		archetypeGearSet.SetGearSetSelections( gearSetSelection );

		JSONArray gearSetSelections = gearObject.getJSONArray( "gearSetSelections" );

		for ( int i = 0; i < gearSetSelections.length(); i++ ){
			JSONObject gearSetSelectionObject = gearSetSelections.getJSONObject( i );
			String gearName = gearSetSelectionObject.getString( "name" );
			
			JSONArray gearListJSON = gearSetSelectionObject.getJSONArray( "gearList" );
			ArrayList<String> gearList = new ArrayList<>();

			for ( int j = 0; j < gearListJSON.length(); j++ ){
				String gearListEntry = gearListJSON.get(j).toString();

				gearList.add( gearListEntry );
			}

			Gear gear = new Gear( gearName, gearList );

			gearSetSelection.add( gear );
		}
		return archetypeGearSet;
	}

	/**
	 * Helper function for adding all the entries in the "improvements" key of the Archetype JSON file.
	 * @param improvementArray
	 * @return ArrayList<String> improvements
	 */
	public ArrayList<String> AddImprovement( JSONArray improvementArray ){
		ArrayList<String> improvements = new ArrayList<>();

		for ( int i = 0; i < improvementArray.length(); i++ ){
			String entry = improvementArray.get(i).toString();

			improvements.add( entry );
		}

		return improvements;
	}

	/**
	 * Helper function for adding all the entries in the "advancedImprovements" key of the Archetype JSON file.
	 * @param advancedImprovementArray
	 * @return ArrayList<String> advancedImprovements
	 */
	public ArrayList<String> AddAdvancedImprovement( JSONArray advancedImprovementArray ){
		ArrayList<String> advancedImprovements = new ArrayList<>();

		for ( int i = 0; i < advancedImprovementArray.length(); i++ ){
			String entry = advancedImprovementArray.get(i).toString();

			advancedImprovements.add( entry );
		}

		return advancedImprovements;
	}

	/**
	 * Helper function for adding all the entries in the "moves" key of the Archetype JSON file.
	 * @param moveObject
	 * @return MoveList moveList
	 */
	public MoveList AddMove( JSONObject moveObject ){
		String desc = moveObject.getString( "desc" );

		JSONArray moveArray = moveObject.getJSONArray( "moveList" );
		MoveList moveList = new MoveList( desc );
		moveList.SetMaxMoveCount( moveObject.getInt( "moveCount" ) );
		
		for ( int i = 0; i < moveArray.length(); i++ ){
			JSONObject move = moveArray.getJSONObject( i );

			String moveName = move.getString( "name" );
			boolean isPreselected = move.getBoolean( "isPreselected" );
			String moveDesc = move.getString( "desc" );

			Move m = new Move( moveName, isPreselected, moveDesc );

			moveList.GetMoveList().add( m );
		}

		return moveList;
	}

	/**
	 * Helper function for adding all the entries in the "ratings" key of the Archetype JSON file.
	 * @param ratingArray
	 * @return ArrayList<Rating> ratings
	*/	
	public ArrayList<Rating> AddRating( JSONArray ratingArray ){
		ArrayList<Rating> ratings = new ArrayList<>();

		for ( int i = 0; i < ratingArray.length(); i++ ){
			JSONObject ratingJSON = ratingArray.getJSONObject(i);

			String ratingName = ratingJSON.getString( "name" );
			int charm = ratingJSON.getInt( "charm" );
			int cool = ratingJSON.getInt( "cool" );
			int sharp = ratingJSON.getInt( "sharp" );
			int tough = ratingJSON.getInt( "tough" );
			int weird = ratingJSON.getInt( "weird" );

			Rating r = new Rating( ratingName, charm, cool, sharp, tough, weird );
			
			ratings.add( r );
		}

		return ratings;
	}

	/**
	 * Helper function for adding all the special mechanics in the "specialMechanics" key of the Archetype JSON file.
	 * @param specialMechanics
	 * @return ArrayList<SpecialMechanic> sm
	 */
	public ArrayList<SpecialMechanic> AddSpecialMechanic( JSONArray specialMechanics ){
		ArrayList<SpecialMechanic> sm = new ArrayList<>();

		for ( int i = 0; i < specialMechanics.length(); i++ ){
			JSONObject specialMechanic = specialMechanics.getJSONObject(i);

			String mechanicName = specialMechanic.getString( "name" );
			// System.out.println( mechanicName );

			String mechanicDesc = specialMechanic.getString( "desc" );
			// System.out.println( mechanicDesc );
			SpecialMechanic specmech = new SpecialMechanic( mechanicName, mechanicDesc );

			JSONArray specialMechanicList = specialMechanic.getJSONArray( "specialMechanicList" );

			for ( Object o : specialMechanicList ){
				String objectIsString = o.toString();
				JSONObject objectIsJSONObject = new JSONObject();
				SpecialMechanicSelection specialMechanicSelection;

				try {	
					objectIsJSONObject = new JSONObject ( new JSONTokener( objectIsString ) );
					specialMechanicSelection = new SpecialMechanicSelection( objectIsJSONObject.has( "name" ) );
				}
				catch ( Exception e ){
					specialMechanicSelection = new SpecialMechanicSelection( false );
				}

				if ( specialMechanicSelection.IsJSONObject() ){
					specialMechanicSelection.SetName( objectIsJSONObject.getString( "name" ) );
					
					JSONArray selection = objectIsJSONObject.getJSONArray( "selection" );

					for ( Object s : selection ){
						specialMechanicSelection.AddEntryToSelection( (String) s );
					}
				}
				else {
					specialMechanicSelection.AddEntryToSelection( objectIsString );
				}

				specmech.AddMechanicToList( specialMechanicSelection );
			}

			sm.add( specmech );
		}

		return sm;
	}

	/**
	 * Processes the files in the archetypes directory and verifies them.
	 * @throws IOException
	 */
	public void VerifyArchetype() throws IOException{
		archetypeData = archetypesFolder.listFiles();

		for ( File archetype : archetypeData ){
			if ( archetype.isFile() ){
				OpenFile( archetype );
				String fileContent = ReadFile( archetype ) ;
				AddArchetype( fileContent );
			}
		}
	}

	public void VerifyCharacter() throws IOException{
		characterData = charactersFolder.listFiles();

		for ( File character : characterData ){
			if ( character.isFile() ){
				OpenFile( character );
				String fileContent = ReadFile( character );
				AddCharacter( fileContent );
			}
		}
	}

	/**
	 * Prints out the basic info of the Archetype.
	 * @param a
	 */
	public void DisplayBasicInfo( Archetype a ){
		System.out.println( a.GetArchetypeName() );
		System.out.println( a.GetArchetypeDesc() );
	}

	/**
	 * Prints out the special mechanics of the Archetype.
	 * @param a
	 */
	public void DisplaySpecialMechanics( Archetype a ){
		System.out.println( "SPECIAL MECHANICS " );

		for ( SpecialMechanic sm : a.GetSpecialMechanics() ){
			System.out.println( sm.GetMechanicName() );
			System.out.println( sm.GetMechanicDesc() );

			for ( SpecialMechanicSelection sms : sm.GetMechanicList() ){
				if ( sms.IsJSONObject() ){
					System.out.println( "  " + sms.GetName() );
				}
				for ( String s : sms.GetSelection() ){
					System.out.println( "    " + s );
				}
			}
		}
	}

	/**
	 * Prints out the gears of the Archetype.
	 * @param a
	 */
	public void DisplayGears( Archetype a ){
		System.out.println( "GEARS" );
		System.out.println( a.GetGearSet().GetGearSetDesc() );

		for ( Gear g : a.GetGearSet().GetGearSetSelections() ){
			System.out.println( g.GetGearName() );

			for ( String s : g.GetGearList() ){
				System.out.println( "  " + s );
			}
		}
	}

	/**
	 * Prints out the moves of the Archetype.
	 * @param a
	 */
	public void DisplayMoves( Archetype a ){
		System.out.println( "MOVES" );

		for ( Move move : a.GetMoveList().GetMoveList() ){
			if ( move.IsPreselected() ){
				System.out.println( "  [PRESELECTED] " + move.GetMoveName() );
				System.out.println( "    " + move.GetMoveDesc() );
				System.out.printf( "\n" );
				
				continue;
			}

			System.out.println( "  " + move.GetMoveName() );
			System.out.println( "    " + move.GetMoveDesc() );
			System.out.printf( "\n" );
		}
	}

	/**
	 * Prints out the ratings of the Archetype.
	 * @param a
	 */
	public void DisplayRatings( Archetype a ){
		System.out.println( "RATINGS" );

		for ( Rating r : a.GetRatings() ){
			System.out.println( r.GetRatingName() );
		}
	}

	/**
	 * Prints out the improvements of the Archetype.
	 * @param a
	 */
	public void DisplayImprovements( Archetype a ){
		System.out.println( "IMPROVEMENTS" );

		for ( String s : a.GetImprovements() ){
			System.out.println( s );
		}
	}

	/**
	 * Prints out the advanced improvements of the Archetype.
	 * @param a
	 */
	public void DisplayAdvancedImprovements( Archetype a ){
		System.out.println( "ADVANCED IMPROVEMENTS" );

		for ( String s : a.GetAdvancedImprovements() ){
			System.out.println( s );
		}
	}

	/**
	 * Prints out the information of the Archetype.
	 * @param a
	 */
	public void DisplayArchetypes(){
		for ( Archetype a : archetypes ){
			System.out.println();
			DisplayBasicInfo( a );
			System.out.println();
			DisplaySpecialMechanics( a );
			System.out.println();
			DisplayMoves( a );
			System.out.println();
			DisplayGears(a);
			System.out.println();
			DisplayRatings(a);
			System.out.println();
			DisplayImprovements(a);
			System.out.println();
			DisplayAdvancedImprovements(a);
			System.out.println("========================================================================================================================");
		}
	}

	public void DisplayCharacters(){
		for ( Character c : characters ){
			System.out.println();
			System.out.println( c.GetCharacterName() );
			System.out.println( c.GetCharacterArchetype().GetName() );
		}
	}

	public void SaveCharacters() throws IOException{
		for ( Character c: characters ){
			System.out.println( "Saving " + c.GetCharacterName() ); 
			WriteCharacterToFile( c );
		}
	}

	/**
	 * Processes and checks the Compendium files for display.
	 * @throws IOException
	 */
	public void VerifyCompendium() throws IOException{
		// compendiumData = compendiumFolder.listFiles();
		File f = new File( compendiumFolder + File.separator + "Compendium.json" );

		OpenFile( f );
		String fileContent = ReadFile( f );
		AddCompendium( fileContent );	
	}

}