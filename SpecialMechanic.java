/**
 * This is the Archetype class of the program.
 * @author Team DM
 * @version 0.1
 */

import java.util.*;
import java.io.*;

public class SpecialMechanic {
	
	// FIELDS
	private String name;
	private String desc;
	private ArrayList<SpecialMechanicSelection> specialMechanicList;

	// CONSTRUCTORS
	public SpecialMechanic(){
		this.name = "";
		this.desc = "";
		this.specialMechanicList = new ArrayList<>();
	}

	public SpecialMechanic( String n, String d ){
		this.name = n;
		this.desc = d;
		this.specialMechanicList = new ArrayList<>();
	}

	// METHODS
	public String GetMechanicName(){
		return this.name;
	}

	public String GetMechanicDesc(){
		return this.desc;
	}

	public ArrayList<SpecialMechanicSelection> GetMechanicList(){
		return this.specialMechanicList;
	}

	public void SetMechanicName( String n ){
		this.name = n;
	}

	public void SetMechanicDesc( String d ){
		this.name = d;
	}

	public void SetMechanicList( ArrayList<SpecialMechanicSelection> objList ){
		this.specialMechanicList = objList;
	}
	
	public void AddMechanicToList( SpecialMechanicSelection obj ){
		this.specialMechanicList.add( obj );
	}

}
