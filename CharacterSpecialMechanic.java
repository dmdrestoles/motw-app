/**
 * This is the Character Special Mechanics class of the program.
 * @author Team DM
 * @version 0.1
 */

import java.util.*;
import java.io.*;

public class CharacterSpecialMechanic {
	
	// FIELDS
	private String name;
	private String desc;
	private String specialMechanicEntry;

	// CONSTRUCTORS
	public CharacterSpecialMechanic(){
		this.name = "";
		this.desc = "";
		this.specialMechanicEntry = "";
	}

	public CharacterSpecialMechanic( String n, String d, String s ){
		this.name = n;
		this.desc = d;
		this.specialMechanicEntry = s;
	}

	// METHODS
	public String GetMechanicName(){
		return this.name;
	}

	public String GetMechanicDesc(){
		return this.desc;
	}

	public String GetSpecialMechanicEntry(){
		return this.specialMechanicEntry;
	}

	public void SetMechanicName( String n ){
		this.name = n;
	}

	public void SetMechanicDesc( String d ){
		this.desc = d;
	}

	public void SetMechanicEntry( String s ){
		this.specialMechanicEntry = s;
	}
}
