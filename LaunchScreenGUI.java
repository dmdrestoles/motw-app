import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import javax.swing.*;
import java.io.*;
import java.util.*;
import javax.imageio.ImageIO;

public class LaunchScreenGUI extends JFrame
{
    public JPanel sidePanel, mainPanel, logoPanel, characterPanel, compendiumPanel;
    public JLabel logoLabel, logoLabelText, topLabel;
    public JButton characters, compendium, newCharacter, characterButton;
    public JButton hunterGuide, chosenButton, crookedButton, divineButton, expertButton,
                   flakeButton, gumshoeButton, hexButton, initiateButton, monstrousButton,
                   mundaneButton, pararomanticButton, professionalButton, searcherButton,
                   spellslingerButton, spookyButton, wrongedButton;
    public Color dViolet, lViolet;
    public Font cantarellB1, cantarellB2, Smythe, Josefin, PTSansR, PTSansR2, PTSansB;
    public BufferedImage logoImage = null, chosenImage=null, crookedImage=null, divineImage=null, expertImage=null,
                        flakeImage=null, initiateImage=null, monstrousImage=null, mundaneImage=null,
                        professionalImage=null, spellslingerImage=null, spookyImage=null, wrongedImage=null,
                        gumshoeImage=null, hexImage=null, pararomanticImage=null, searcherImage=null; 
    public Image scaledLogoImage, scaledChosenImage, scaledCrookedImage, scaledDivineImage, scaledExpertImage, 
                scaledFlakeImage, scaledInitiateImage, scaledMonstrousImage, scaledMundaneImage, 
                scaledProfessionalImage, scaledSpellslingerImage, scaledSpookyImage, scaledWrongedImage, 
                scaledGumshoeImage, scaledHexImage, scaledPararomanticImage, scaledSearcherImage;
    public Container Screen;
    public CharacterCreationGUI newChar;
    public CompendiumContentLoaderGUI compendiumContent;
    public CharacterSheetGUI characterSheet;
    public FileLoader fl;
    public Character loadedCharacter;
    public ArrayList<Character> characterList;

    public LaunchScreenGUI()
    {
        dViolet = new Color(19, 11, 16);
        lViolet = new Color(48, 28, 41);

        cantarellB1 = new Font("Cantarell-Bold", Font.BOLD, 15);
        cantarellB2 = new Font("Cantarell-Bold", Font.BOLD, 20);
        Smythe = new Font("Smythe", Font.BOLD, 20);
        Josefin = new Font("Josefin Sans", Font.PLAIN, 50);
        PTSansB = new Font("PT Sans", Font.BOLD, 20);
        PTSansR = new Font("PT Sans", Font.ITALIC, 20);
        PTSansR2 = new Font("PT Sans", Font.ITALIC, 12);
        
        try
        {
            logoImage = ImageIO.read(new File("./images/logo.png"));
            scaledLogoImage = logoImage.getScaledInstance(177, 38, Image.SCALE_DEFAULT);

            chosenImage = ImageIO.read(new File("./images/Chosen.png"));
            scaledChosenImage = chosenImage.getScaledInstance(150, 150, Image.SCALE_DEFAULT);

            crookedImage = ImageIO.read(new File("./images/Crooked.png"));
            scaledCrookedImage = crookedImage.getScaledInstance(150, 150, Image.SCALE_DEFAULT);

            divineImage = ImageIO.read(new File("./images/Divine.png"));
            scaledDivineImage = divineImage.getScaledInstance(150, 150, Image.SCALE_DEFAULT);

            expertImage = ImageIO.read(new File("./images/Expert.png"));
            scaledExpertImage = expertImage.getScaledInstance(150, 150, Image.SCALE_DEFAULT);

            flakeImage = ImageIO.read(new File("./images/Flake.png"));
            scaledFlakeImage = flakeImage.getScaledInstance(150, 150, Image.SCALE_DEFAULT);

            initiateImage = ImageIO.read(new File("./images/Initiate.png"));
            scaledInitiateImage = initiateImage.getScaledInstance(150, 150, Image.SCALE_DEFAULT);

            monstrousImage = ImageIO.read(new File("./images/Monstrous.png"));
            scaledMonstrousImage = monstrousImage.getScaledInstance(150, 150, Image.SCALE_DEFAULT);

            mundaneImage = ImageIO.read(new File("./images/Mundane.png"));
            scaledMundaneImage = mundaneImage.getScaledInstance(150, 150, Image.SCALE_DEFAULT);

            professionalImage = ImageIO.read(new File("./images/Professional.png"));
            scaledProfessionalImage = professionalImage.getScaledInstance(150, 150, Image.SCALE_DEFAULT);

            spellslingerImage = ImageIO.read(new File("./images/Spellslinger.png"));
            scaledSpellslingerImage = spellslingerImage.getScaledInstance(150, 150, Image.SCALE_DEFAULT);

            spookyImage = ImageIO.read(new File("./images/Spooky.png"));
            scaledSpookyImage = spookyImage.getScaledInstance(150, 150, Image.SCALE_DEFAULT);

            wrongedImage = ImageIO.read(new File("./images/Wronged.png"));
            scaledWrongedImage = wrongedImage.getScaledInstance(150, 150, Image.SCALE_DEFAULT);

            gumshoeImage = ImageIO.read(new File("./images/Gumshoe.png"));
            scaledGumshoeImage = gumshoeImage.getScaledInstance(150, 150, Image.SCALE_DEFAULT);

            hexImage = ImageIO.read(new File("./images/Hex.png"));
            scaledHexImage = hexImage.getScaledInstance(150, 150, Image.SCALE_DEFAULT);

            pararomanticImage = ImageIO.read(new File("./images/Pararomantic.png"));
            scaledPararomanticImage = pararomanticImage.getScaledInstance(150, 150, Image.SCALE_DEFAULT);

            searcherImage = ImageIO.read(new File("./images/Searcher.png"));
            scaledSearcherImage = searcherImage.getScaledInstance(150, 150, Image.SCALE_DEFAULT);
        }
        catch (IOException ex)
        {
            System.out.println("File Not Found");
        }


        fl = new FileLoader();
        try {	
			fl.VerifyCharacter();
            fl.VerifyCompendium();
		}
		catch (IOException ioe){
			ioe.printStackTrace();
		}

        loadedCharacter = new Character();
        characterList = new ArrayList<Character>();

        Screen = this.getContentPane();

        characterSelection();
    }

    public void characterSelection()
    {
        characterButton = new JButton();
        characterButton.setPreferredSize(new Dimension(220, 304));
        characterButton.setFont(PTSansR);
        characterButton.setBackground(dViolet);
        characterButton.setForeground(Color.WHITE);

        newCharacter = new JButton("+ Add New Character");
        newCharacter.setPreferredSize(new Dimension(220, 304));
        newCharacter.setFont(PTSansR);
        newCharacter.setBackground(dViolet);
        newCharacter.setForeground(Color.WHITE);

        logoLabel = new JLabel(new ImageIcon(scaledLogoImage), JLabel.CENTER);
        logoLabel.setBorder(null);

        logoLabelText = new JLabel("Hunter Companion", JLabel.CENTER);
        logoLabelText.setFont(Smythe);
        logoLabelText.setPreferredSize(new Dimension(256, 20));
        logoLabelText.setForeground(Color.WHITE);

        sidePanel = new JPanel(new FlowLayout(FlowLayout.CENTER, -20, 15));
        characterPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 20, 20));
        mainPanel = new JPanel(new BorderLayout());
        logoPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, -85, 0)); 

        sidePanel.setPreferredSize(new Dimension(256, 1024));
        logoPanel.setPreferredSize(new Dimension(256, 100));

        mainPanel.setMinimumSize(new Dimension(1184, 1024));

        topLabel = new JLabel("   YOUR CHARACTERS");

        characters = new JButton("CHARACTERS");
        compendium = new JButton("COMPENDIUM");

        characters.setPreferredSize(new Dimension(256, 50));
        characters.setBorder(null);
        characters.setForeground(Color.WHITE);

        compendium.setPreferredSize(new Dimension(256, 50));
        compendium.setBorder(null);
        compendium.setForeground(Color.WHITE);

        characters.setFont(cantarellB2);
        compendium.setFont(cantarellB2);

        sidePanel.setBackground(dViolet);
        mainPanel.setBackground(lViolet);
        logoPanel.setBackground(dViolet);
        characterPanel.setBackground(lViolet);

        Screen.setLayout(new GridBagLayout());

        characters.setBackground(lViolet);
        compendium.setBackground(dViolet);

        topLabel.setText("   YOUR CHARACTERS");
        topLabel.setFont(Josefin);
        topLabel.setPreferredSize(new Dimension(1184, 100));
        topLabel.setForeground(Color.WHITE);

        mainPanel.add(topLabel, BorderLayout.NORTH);
        mainPanel.add(characterPanel, BorderLayout.CENTER);

        logoPanel.add(logoLabel);
        logoPanel.add(logoLabelText);

        sidePanel.add(logoPanel);
        sidePanel.add(characters);
        sidePanel.add(compendium);

        for (Character a: fl.GetCharacters())
        {
            characterList.add(a);
        }

        class CharacterSheetButton implements ActionListener
        {
            public void actionPerformed(ActionEvent ae)
            {
                mainPanel.removeAll();
                sidePanel.removeAll();
                Screen.removeAll();
                Object source = ae.getSource();
                JButton button = (JButton) source;
                characterSheet = new CharacterSheetGUI(button.getText());
                characterSheet.setSize(1440, 1024);
                String s = button.getText();
                characterSheet.setTitle("Monster of the Week Hunter Companion | " + s);
                characterSheet.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                characterSheet.setVisible(true);
                dispose();
                //compendiumContent.CompendiumSelection(Screen, button.getText());
                revalidate();
                repaint();
            }
        }

        for (Character c: characterList)
        {
            characterButton = new JButton();
            characterButton.setPreferredSize(new Dimension(220, 304));
            characterButton.setFont(PTSansR);
            characterButton.setBackground(dViolet);
            characterButton.setForeground(Color.WHITE);
            characterButton.setText(c.GetCharacterName());
            characterButton.addActionListener(new CharacterSheetButton());
            characterPanel.add(characterButton);
        }

        characterPanel.add(newCharacter);
        
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.VERTICAL;
        constraints.gridx = 0;
        constraints.gridy = 0;
        constraints.gridwidth = 1;
        constraints.anchor = GridBagConstraints.FIRST_LINE_START;
        Screen.add(sidePanel, constraints);

        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.gridx = 1;
        constraints.gridy = 0;
        constraints.gridwidth = 4;
        constraints.ipadx = 0;
        constraints.ipady = 0;
        constraints.anchor = GridBagConstraints.PAGE_START;
        Screen.add(mainPanel, constraints);

        class CompendiumButton implements ActionListener
        {
            public void actionPerformed(ActionEvent ae)
            {
                mainPanel.removeAll();
                sidePanel.removeAll();
                Screen.removeAll();
                characterList.clear();
                compendium();
                revalidate();
                repaint();
            }
        }
        compendium.addActionListener(new CompendiumButton());

        class NewCharacterButton implements ActionListener
        {
            public void actionPerformed(ActionEvent ae)
            {
                mainPanel.removeAll();
                sidePanel.removeAll();
                Screen.removeAll();
                newChar = new CharacterCreationGUI();
                newChar.setSize(1440, 1024);
                newChar.setTitle("Monster of the Week Hunter Companion | New Character");
                newChar.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                newChar.setVisible(true);
                dispose();
                revalidate();
                repaint();
            }
        }
        newCharacter.addActionListener(new NewCharacterButton());
        
    }

    public void compendium()
    {
        hunterGuide = new JButton("Hunter Guide Book");
        
        chosenButton = new JButton("The Chosen", new ImageIcon(scaledChosenImage));
        chosenButton.setVerticalTextPosition(SwingConstants.BOTTOM);
        chosenButton.setHorizontalTextPosition(SwingConstants.CENTER);

        crookedButton = new JButton("The Crooked", new ImageIcon(scaledCrookedImage));
        crookedButton.setVerticalTextPosition(SwingConstants.BOTTOM);
        crookedButton.setHorizontalTextPosition(SwingConstants.CENTER);

        divineButton = new JButton("The Divine", new ImageIcon(scaledDivineImage));
        divineButton.setVerticalTextPosition(SwingConstants.BOTTOM);
        divineButton.setHorizontalTextPosition(SwingConstants.CENTER);

        expertButton = new JButton("The Expert", new ImageIcon(scaledExpertImage));
        expertButton.setVerticalTextPosition(SwingConstants.BOTTOM);
        expertButton.setHorizontalTextPosition(SwingConstants.CENTER);

        flakeButton = new JButton("The Flake", new ImageIcon(scaledFlakeImage));
        flakeButton.setVerticalTextPosition(SwingConstants.BOTTOM);
        flakeButton.setHorizontalTextPosition(SwingConstants.CENTER);

        gumshoeButton = new JButton("The Gumshoe", new ImageIcon(scaledGumshoeImage));
        gumshoeButton.setVerticalTextPosition(SwingConstants.BOTTOM);
        gumshoeButton.setHorizontalTextPosition(SwingConstants.CENTER);

        hexButton = new JButton("The Hex", new ImageIcon(scaledHexImage));
        hexButton.setVerticalTextPosition(SwingConstants.BOTTOM);
        hexButton.setHorizontalTextPosition(SwingConstants.CENTER);

        initiateButton = new JButton("The Initiate", new ImageIcon(scaledInitiateImage));
        initiateButton.setVerticalTextPosition(SwingConstants.BOTTOM);
        initiateButton.setHorizontalTextPosition(SwingConstants.CENTER);

        monstrousButton = new JButton("The Monstrous", new ImageIcon(scaledMonstrousImage));
        monstrousButton.setVerticalTextPosition(SwingConstants.BOTTOM);
        monstrousButton.setHorizontalTextPosition(SwingConstants.CENTER);

        mundaneButton = new JButton("The Mundane", new ImageIcon(scaledMundaneImage));
        mundaneButton.setVerticalTextPosition(SwingConstants.BOTTOM);
        mundaneButton.setHorizontalTextPosition(SwingConstants.CENTER);

        pararomanticButton = new JButton("The Pararomantic", new ImageIcon(scaledPararomanticImage));
        pararomanticButton.setVerticalTextPosition(SwingConstants.BOTTOM);
        pararomanticButton.setHorizontalTextPosition(SwingConstants.CENTER);

        professionalButton = new JButton("The Professional", new ImageIcon(scaledProfessionalImage));
        professionalButton.setVerticalTextPosition(SwingConstants.BOTTOM);
        professionalButton.setHorizontalTextPosition(SwingConstants.CENTER);

        searcherButton = new JButton("The Searcher", new ImageIcon(scaledSearcherImage));
        searcherButton.setVerticalTextPosition(SwingConstants.BOTTOM);
        searcherButton.setHorizontalTextPosition(SwingConstants.CENTER);

        spellslingerButton = new JButton("The Spell-Slinger", new ImageIcon(scaledSpellslingerImage));
        spellslingerButton.setVerticalTextPosition(SwingConstants.BOTTOM);
        spellslingerButton.setHorizontalTextPosition(SwingConstants.CENTER);

        spookyButton = new JButton("The Spooky", new ImageIcon(scaledSpookyImage));
        spookyButton.setVerticalTextPosition(SwingConstants.BOTTOM);
        spookyButton.setHorizontalTextPosition(SwingConstants.CENTER);

        wrongedButton = new JButton("The Wronged", new ImageIcon(scaledWrongedImage));
        wrongedButton.setVerticalTextPosition(SwingConstants.BOTTOM);
        wrongedButton.setHorizontalTextPosition(SwingConstants.CENTER);
        hunterGuide.setPreferredSize(new Dimension(170, 200));
        hunterGuide.setFont(PTSansR2);
        hunterGuide.setBackground(dViolet);
        hunterGuide.setForeground(Color.WHITE);

        chosenButton.setPreferredSize(new Dimension(170, 200));
        chosenButton.setFont(PTSansR2);
        chosenButton.setBackground(dViolet);
        chosenButton.setForeground(Color.WHITE);

        crookedButton.setPreferredSize(new Dimension(170, 200));
        crookedButton.setFont(PTSansR2);
        crookedButton.setBackground(dViolet);
        crookedButton.setForeground(Color.WHITE);

        divineButton.setPreferredSize(new Dimension(170, 200));
        divineButton.setFont(PTSansR2);
        divineButton.setBackground(dViolet);
        divineButton.setForeground(Color.WHITE);

        expertButton.setPreferredSize(new Dimension(170, 200));
        expertButton.setFont(PTSansR2);
        expertButton.setBackground(dViolet);
        expertButton.setForeground(Color.WHITE);
        
        flakeButton.setPreferredSize(new Dimension(170, 200));
        flakeButton.setFont(PTSansR2);
        flakeButton.setBackground(dViolet);
        flakeButton.setForeground(Color.WHITE);

        gumshoeButton.setPreferredSize(new Dimension(170, 200));
        gumshoeButton.setFont(PTSansR2);
        gumshoeButton.setBackground(dViolet);
        gumshoeButton.setForeground(Color.WHITE);

        hexButton.setPreferredSize(new Dimension(170, 200));
        hexButton.setFont(PTSansR2);
        hexButton.setBackground(dViolet);
        hexButton.setForeground(Color.WHITE);
        
        initiateButton.setPreferredSize(new Dimension(170, 200));
        initiateButton.setFont(PTSansR2);
        initiateButton.setBackground(dViolet);
        initiateButton.setForeground(Color.WHITE);

        monstrousButton.setPreferredSize(new Dimension(170, 200));
        monstrousButton.setFont(PTSansR2);
        monstrousButton.setBackground(dViolet);
        monstrousButton.setForeground(Color.WHITE);
        
        mundaneButton.setPreferredSize(new Dimension(170, 200));
        mundaneButton.setFont(PTSansR2);
        mundaneButton.setBackground(dViolet);
        mundaneButton.setForeground(Color.WHITE);

        pararomanticButton.setPreferredSize(new Dimension(170, 200));
        pararomanticButton.setFont(PTSansR2);
        pararomanticButton.setBackground(dViolet);
        pararomanticButton.setForeground(Color.WHITE);

        professionalButton.setPreferredSize(new Dimension(170, 200));
        professionalButton.setFont(PTSansR2);
        professionalButton.setBackground(dViolet);
        professionalButton.setForeground(Color.WHITE);

        searcherButton.setPreferredSize(new Dimension(170, 200));
        searcherButton.setFont(PTSansR2);
        searcherButton.setBackground(dViolet);
        searcherButton.setForeground(Color.WHITE);

        spellslingerButton.setPreferredSize(new Dimension(170, 200));
        spellslingerButton.setFont(PTSansR2);
        spellslingerButton.setBackground(dViolet);
        spellslingerButton.setForeground(Color.WHITE);

        spookyButton.setPreferredSize(new Dimension(170, 200));
        spookyButton.setFont(PTSansR2);
        spookyButton.setBackground(dViolet);
        spookyButton.setForeground(Color.WHITE);

        wrongedButton.setPreferredSize(new Dimension(170, 200));
        wrongedButton.setFont(PTSansR2);
        wrongedButton.setBackground(dViolet);
        wrongedButton.setForeground(Color.WHITE);

        compendiumPanel = new JPanel(new FlowLayout());
        compendiumPanel.setBackground(lViolet);

        compendiumPanel.add(hunterGuide);
        compendiumPanel.add(chosenButton);
        compendiumPanel.add(crookedButton);
        compendiumPanel.add(divineButton);
        compendiumPanel.add(expertButton);
        compendiumPanel.add(flakeButton);
        compendiumPanel.add(gumshoeButton);
        compendiumPanel.add(hexButton);
        compendiumPanel.add(initiateButton);
        compendiumPanel.add(monstrousButton);
        compendiumPanel.add(mundaneButton);
        compendiumPanel.add(pararomanticButton);
        compendiumPanel.add(professionalButton);
        compendiumPanel.add(searcherButton);
        compendiumPanel.add(spellslingerButton);
        compendiumPanel.add(spookyButton);
        compendiumPanel.add(wrongedButton);



        logoLabel = new JLabel(new ImageIcon(scaledLogoImage), JLabel.CENTER);
        logoLabel.setBorder(null);

        logoLabelText = new JLabel("Hunter Companion", JLabel.CENTER);
        logoLabelText.setFont(Smythe);
        logoLabelText.setPreferredSize(new Dimension(256, 20));
        logoLabelText.setForeground(Color.WHITE);

        sidePanel = new JPanel(new FlowLayout(FlowLayout.CENTER, -20, 15));
        characterPanel = new JPanel(new FlowLayout(FlowLayout.LEFT, 20, 20));
        mainPanel = new JPanel(new BorderLayout());
        logoPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, -85, 0)); 

        sidePanel.setPreferredSize(new Dimension(256, 1024));
        logoPanel.setPreferredSize(new Dimension(256, 100));

        mainPanel.setMinimumSize(new Dimension(1184, 1024));

        topLabel = new JLabel("   YOUR CHARACTERS");

        characters = new JButton("CHARACTERS");
        compendium = new JButton("COMPENDIUM");

        characters.setPreferredSize(new Dimension(256, 50));
        characters.setBorder(null);
        characters.setForeground(Color.WHITE);

        compendium.setPreferredSize(new Dimension(256, 50));
        compendium.setBorder(null);
        compendium.setForeground(Color.WHITE);

        characters.setFont(cantarellB2);
        compendium.setFont(cantarellB2);

        sidePanel.setBackground(dViolet);
        mainPanel.setBackground(lViolet);
        logoPanel.setBackground(dViolet);
        characterPanel.setBackground(lViolet);

        Screen.setLayout(new GridBagLayout());

        characters.setBackground(dViolet);
        compendium.setBackground(lViolet);

        topLabel.setText("   COMPENDIUM");
        topLabel.setFont(Josefin);
        topLabel.setPreferredSize(new Dimension(1184, 100));
        topLabel.setForeground(Color.WHITE);

        mainPanel.add(topLabel, BorderLayout.NORTH);
        mainPanel.add(compendiumPanel, BorderLayout.CENTER);

        logoPanel.add(logoLabel);
        logoPanel.add(logoLabelText);

        sidePanel.add(logoPanel);
        sidePanel.add(characters);
        sidePanel.add(compendium);
        
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.fill = GridBagConstraints.VERTICAL;
        constraints.gridx = 0;
        constraints.gridy = 0;
        constraints.gridwidth = 1;
        constraints.anchor = GridBagConstraints.FIRST_LINE_START;
        Screen.add(sidePanel, constraints);

        constraints.fill = GridBagConstraints.HORIZONTAL;
        constraints.gridx = 1;
        constraints.gridy = 0;
        constraints.gridwidth = 4;
        constraints.ipadx = 0;
        constraints.ipady = 0;
        constraints.anchor = GridBagConstraints.PAGE_START;
        Screen.add(mainPanel, constraints);


        class CharacterButton implements ActionListener
        {
            public void actionPerformed(ActionEvent ae)
            {
                mainPanel.removeAll();
                sidePanel.removeAll();
                Screen.removeAll();
                characterSelection();
                revalidate();
                repaint();
            }
        }
        characters.addActionListener(new CharacterButton());
        
        class CompendiumInfoButton implements ActionListener
        {
            public void actionPerformed(ActionEvent ae)
            {
                mainPanel.removeAll();
                sidePanel.removeAll();
                Screen.removeAll();
                Object source = ae.getSource();
                JButton button = (JButton) source;
                compendiumContent = new CompendiumContentLoaderGUI(button.getText());
                String s = button.getText();
                compendiumContent.setSize(1440, 1024);
                compendiumContent.setTitle("Monster of the Week Hunter Companion | " + s);
                compendiumContent.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                compendiumContent.setVisible(true);
                dispose();
                //compendiumContent.CompendiumSelection(Screen, button.getText());
                revalidate();
                repaint();
            }
        }
        hunterGuide.addActionListener(new CompendiumInfoButton());
        chosenButton.addActionListener(new CompendiumInfoButton());
        crookedButton.addActionListener(new CompendiumInfoButton());
        divineButton.addActionListener(new CompendiumInfoButton());
        expertButton.addActionListener(new CompendiumInfoButton());
        flakeButton.addActionListener(new CompendiumInfoButton());
        gumshoeButton.addActionListener(new CompendiumInfoButton());
        hexButton.addActionListener(new CompendiumInfoButton());
        initiateButton.addActionListener(new CompendiumInfoButton());
        monstrousButton.addActionListener(new CompendiumInfoButton());
        mundaneButton.addActionListener(new CompendiumInfoButton());
        pararomanticButton.addActionListener(new CompendiumInfoButton());
        professionalButton.addActionListener(new CompendiumInfoButton());
        searcherButton.addActionListener(new CompendiumInfoButton());
        spellslingerButton.addActionListener(new CompendiumInfoButton());
        spookyButton.addActionListener(new CompendiumInfoButton());
        wrongedButton.addActionListener(new CompendiumInfoButton());

    }
}

