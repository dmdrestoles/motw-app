/**
 * This is the Move class of the program.
 * @author Team DM
 * @version 0.1
 */

import java.util.*;
import java.io.*;

public class Move {
	
	// FIELDS
	private String name;
	private boolean isPreselected;
	private String desc;

	// CONSTRUCTORS
	public Move(){
		this.name = "";
		this.isPreselected = false;
		this.desc = "";
	}

	public Move( String n, boolean i, String d ){
		this.name = n;
		this.isPreselected = i;
		this.desc = d;
	}

	// METHODS
	public String GetMoveName(){
		return this.name;
	}

	public boolean IsPreselected(){
		return this.isPreselected;
	}

	public String GetMoveDesc(){
		return this.desc;
	}

	public void SetMoveName( String n ){
		this.name = n;
	}

	public void SetPreselected( boolean i ){
		this.isPreselected = i;
	}

	public void SetMoveDesc( String d ){
		this.desc = d;
	}


}
