import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import javax.swing.*;
import java.io.*;
import java.util.*;
import javax.imageio.ImageIO;

public class CompendiumContentLoaderGUI extends JFrame
{
    public JButton returnButton, nextPage, previousPage;
    public JPanel pagePanel, buttonPanel;
    public JLabel showPage;
    public String compendiumImageString;
    public int numberOfPages, currentPage;
    public BufferedImage page1, page2, page3, page4;
    public Image scaledPage1, scaledPage2, scaledPage3, scaledPage4;
    public ArrayList<Image> pageArray;
    public LaunchScreenGUI launchScreen;
    public Color dViolet, lViolet;
    public Container Screen;
    public Font ptSansBig;

    public CompendiumContentLoaderGUI(String s)
    {
        dViolet = new Color(19, 11, 16);
        lViolet = new Color(48, 28, 41);

        ptSansBig = new Font("PT Sans", Font.BOLD, 30);

        numberOfPages = 1;
        currentPage = 0;

        pageArray = new ArrayList<Image>();

        Screen = this.getContentPane();

        CompendiumSelection(Screen , s);
    }

    public void CompendiumSelection(Container s, String buttonString)
    {
        switch(buttonString)
        {
            case "Hunter Guide Book":
                compendiumImageString = "HunterGuideBook";
                break;
            case "The Chosen":
                compendiumImageString = "Chosen";
                break;
            case "The Crooked":
                compendiumImageString = "Crooked";
                break;
            case "The Divine":
                compendiumImageString = "Divine";
                break;
            case "The Expert":
                compendiumImageString = "Expert";
                break;
            case "The Flake":
                compendiumImageString = "Flake";
                break;
            case "The Gumshoe":
                compendiumImageString = "Gumshoe";
                break;
            case "The Hex":
                compendiumImageString = "Hex";
                numberOfPages = 3;
                break;
            case "The Initiate":
                compendiumImageString = "Initiate";
                break;
            case "The Monstrous":
                compendiumImageString = "Monstrous";
                break;
            case "The Mundane":
                compendiumImageString = "Mundane";
                break;
            case "The Pararomantic":
                compendiumImageString = "Pararomantic";
                break;
            case "The Professional":
                compendiumImageString = "Professional";
                break;
            case "The Searcher":
                compendiumImageString = "Searcher";
                break;
            case "The Spell-Slinger":
                compendiumImageString = "SpellSlinger";
                break;
            case "The Spooky":
                compendiumImageString = "Spooky";
                break;
            case "The Wronged":
                compendiumImageString = "Wronged";
                break;
        }

        try
        {
            page1 = ImageIO.read(new File("./compendium/" + compendiumImageString + "_1.jpg"));
            page2 = ImageIO.read(new File("./compendium/" + compendiumImageString + "_2.jpg"));
            
            if(numberOfPages > 1)
            {
                page3 = ImageIO.read(new File("./compendium/" + compendiumImageString + "_3.jpg"));
                page4 = ImageIO.read(new File("./compendium/" + compendiumImageString + "_4.jpg"));
            }
        }
        catch (IOException ex)
        {
            System.out.println("File Not Found");
        }

        scaledPage1 = page1.getScaledInstance(940, 726, Image.SCALE_AREA_AVERAGING);
        pageArray.add(scaledPage1);
        scaledPage2 = page2.getScaledInstance(940, 726, Image.SCALE_AREA_AVERAGING);
        pageArray.add(scaledPage2);

        if(numberOfPages > 2)
        {
            scaledPage3 = page3.getScaledInstance(940, 726, Image.SCALE_AREA_AVERAGING);
            pageArray.add(scaledPage3);
            scaledPage4 = page4.getScaledInstance(940, 726, Image.SCALE_AREA_AVERAGING);
            pageArray.add(scaledPage4);
        }
        
        CompendiumContentScreen(Screen);
    }

    private void CompendiumContentScreen(Container Screen)
    {
        revalidate();
        repaint();

        returnButton = new JButton("Return to Menu");
        previousPage = new JButton("Previous Page");
        nextPage = new JButton("Next Page");

        returnButton.setBackground(lViolet);
        returnButton.setForeground(Color.WHITE);
        previousPage.setBackground(lViolet);
        previousPage.setForeground(Color.WHITE);
        nextPage.setBackground(lViolet);
        nextPage.setForeground(Color.WHITE);

        returnButton.setFont(ptSansBig);
        previousPage.setFont(ptSansBig);
        nextPage.setFont(ptSansBig);

        buttonPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 20, 0));
        buttonPanel.setBackground(dViolet);
        buttonPanel.add(previousPage);
        buttonPanel.add(nextPage);
        buttonPanel.add(returnButton);

        showPage = new JLabel(new ImageIcon(pageArray.get(currentPage)), JLabel.CENTER);

        pagePanel = new JPanel();
        pagePanel.setBackground(dViolet);

        pagePanel.add(showPage);

        Screen.setBackground(dViolet);
        Screen.setLayout(new BorderLayout());
        Screen.add(pagePanel, BorderLayout.NORTH);
        Screen.add(buttonPanel, BorderLayout.CENTER);

        class PreviousPageButton implements ActionListener
        {
            public void actionPerformed(ActionEvent ae)
            {
                pagePanel.removeAll();
                Screen.remove(pagePanel);
                showPage.setIcon(null);

                if (currentPage > 0)
                {
                    currentPage -= 1;
                }

                showPage = new JLabel(new ImageIcon(pageArray.get(currentPage)), JLabel.CENTER);
                pagePanel.add(showPage);
                Screen.add(pagePanel, BorderLayout.CENTER);
                revalidate();
                repaint();
            }
        }
        previousPage.addActionListener(new PreviousPageButton());

        class NextPageButton implements ActionListener
        {
            public void actionPerformed(ActionEvent ae)
            {
                pagePanel.removeAll();
                Screen.remove(pagePanel);
                showPage.setIcon(null);
                
                if (currentPage < numberOfPages)
                {
                    currentPage += 1;
                }

                showPage = new JLabel(new ImageIcon(pageArray.get(currentPage)), JLabel.CENTER);
                pagePanel.add(showPage);
                Screen.add(pagePanel, BorderLayout.CENTER);
                revalidate();
                repaint();
            }
        }
        nextPage.addActionListener(new NextPageButton());

        class ReturnButton implements ActionListener
        {
            public void actionPerformed(ActionEvent ae)
            {
                Screen.removeAll();
                pagePanel.removeAll();
                buttonPanel.removeAll();
                launchScreen = new LaunchScreenGUI();
                launchScreen.setSize(1440, 1024);
                launchScreen.setTitle("Monster of the Week Hunter Companion");
                launchScreen.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                launchScreen.setVisible(true);
                dispose();
                revalidate();
                repaint();
            }
        }
        returnButton.addActionListener(new ReturnButton());
    }
}
