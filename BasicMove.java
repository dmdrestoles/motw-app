/**
 * This is the BasicMove class of the program.
 * @author Team DM
 * @version 0.1
 */

import java.util.*;
import java.io.*;

public class BasicMove {
	
	public String name;
	public String desc;
	public String on7;
	public String on10;
	public String advanced;

	public BasicMove(){
		this.name = "";
		this.desc = "";
		this.on7 = "";
		this.on10 = "";
		this.advanced = "";
	}
}
