import java.io.IOException;
import javax.swing.*;

/**
 * This is the main file for running the program.
 * @author Team DM
 * @version 0.1
 */

import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import javax.swing.*;
import java.io.*;
import java.util.*;

public class App{

	public static void main( String[] args ){
		// FileLoader fl = new FileLoader();
		/*
		try {	
			fl.VerifyArchetype();
			fl.VerifyCharacter();
			// fl.DisplayCharacters();
			// fl.SaveCharacters();
			// fl.DisplayArchetypes();
			// fl.VerifyCompendium();
		}
		catch (IOException ioe){
			ioe.printStackTrace();
		}
		*/
		JFrame motwStartup = new LaunchScreenGUI();
		motwStartup.setSize(1440, 1024);
		motwStartup.setTitle("Monster of the Week Hunter Companion");
		motwStartup.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		motwStartup.setVisible(true);
	}

}